import org.hamcrest.Matchers;
import org.hamcrest.core.Every;
import org.junit.*;
import org.junit.internal.MethodSorter;
import org.openqa.selenium.*;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.Select;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.TimeUnit;

public class LeadTests {

    WebDriver driver;
    Methods methods;
    Leads leads;
    ClientPage clientPage;

    @Rule
    public ScreenshotRule screenshotRule = new ScreenshotRule();


    @Before
    public void setUp() throws Exception {
        driver = new ChromeDriver();
        screenshotRule.setDriver(driver);
        driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
        driver.manage().window().maximize();
        driver.get(Methods.getVariable("AdminPanelURL"));
        methods = new Methods(driver);
        leads = new Leads(driver);
        clientPage = new ClientPage(driver);
    }

    @Test
    public void alphaLead() throws Exception {
        String code = "+995 Georgia (საქართველო)";
        List channelItems = new ArrayList();
        Collections.addAll(channelItems, "Phone", "Email", "Website", "Telegram", "Carfax", "Facebook Messenger", "Rajdeba", "Facebook");
        leads.login(Methods.getVariable("Platform"), Methods.getVariable("AdminLogin"), Methods.getVariable("AdminPassword"));
        leads.ifTestLeadMissing(driver, leads.allLeadsByName, Methods.getVariable("UserFullName"), "+995 Georgia (საქართველო)");
        String counterBefore = methods.getElementText(driver, leads.leadRequestCount);
        int counterBeforeInt = Integer.parseInt(counterBefore);
        System.out.println(counterBefore);
        methods.clickElement(driver, leads.createLeadBtn);
        methods.waitMethMS(500);
        methods.clickElement(driver, leads.channelDropdown);
        methods.waitForAllElementsAreVisible(leads.channelDropdownList, 10);
        ArrayList<String> actualChannelItems = Methods.getItemsList(driver, leads.channelDropdownList);
        Assert.assertEquals(channelItems, actualChannelItems);
        methods.clickElement(driver, leads.channelDropdown);

        methods.inputValue(driver, Methods.getVariable("UserFullName"), leads.userFullNameFld);
        methods.clickElement(driver, leads.countryCodesBtn);
        Methods.chooseItemFromTheList(driver, leads.countryCodesLst, code);
        methods.inputValue(driver, Methods.getVariable("UserPhoneNoCode"), leads.phoneNumberFld);
        methods.clickElement(driver, leads.createNewLeadBtn);
        methods.waitForElementIsVisible(leads.leadHeaderUserName, 10);
        String userName = methods.getElementText(driver, leads.leadHeaderUserName);
        Assert.assertEquals(Methods.getVariable("UserFullName"), userName);
        methods.clickElement(driver, leads.closeLeadModal);
        methods.waitMethMS(1000);
        String counterAfter = methods.getElementText(driver, leads.leadRequestCount);
        int counterAfterInt = Integer.parseInt(counterAfter);
        System.out.println(counterAfter);
        Assert.assertTrue(counterAfterInt == counterBeforeInt + 1);
    }

    @Test
    public void leadStatesAndManaging() throws Exception {
        List channelItems = new ArrayList();
        Collections.addAll(channelItems, "Phone", "Email", "Website", "Telegram", "Carfax", "Facebook Messenger", "Rajdeba", "Facebook");
        leads.login(Methods.getVariable("Platform"), Methods.getVariable("AdminLogin"), Methods.getVariable("AdminPassword"));
        List<WebElement> whiteLeadsLeast = driver.findElements(leads.allLeadsConcat);
        String leadClass = whiteLeadsLeast.get(0).getAttribute("class");
        Assert.assertEquals(Methods.getVariable("RegularLeadClass"), leadClass);

        methods.clickElement(driver, leads.testLead);
        methods.clickElement(driver, leads.channelDropdown);
        methods.waitForAllElementsAreVisible(leads.channelDropdownList, 10);
        ArrayList<String> actualChannelItems = Methods.getItemsList(driver, leads.channelDropdownList);
        Assert.assertEquals(channelItems, actualChannelItems);
        methods.clickElement(driver, leads.channelDropdown);
        String emailFld = methods.getCustomAttribute(driver, leads.leadModalEmailFld, "readonly");
        System.out.println(emailFld);
        Assert.assertEquals("true", emailFld);
        String locationFld = methods.getCustomAttribute(driver, leads.locationFld, "readonly");
        Assert.assertEquals("true", locationFld);
        methods.clickElement(driver, leads.closeLeadModal);

        leads.switchLeadStatus(driver, leads.testLead, leads.financingToggleYes);
        methods.waitMethMS(200);
        methods.clickElement(driver, leads.closeLeadModal);
        List<WebElement> greenLeadsList = driver.findElements(leads.allLeadsConcat);
        List<WebElement> leadsList = driver.findElements(leads.allLeadsByName);
        String leadFinancialClass = greenLeadsList.get(0).getAttribute("class");
        String name = leadsList.get(0).getText();
        System.out.println(leadFinancialClass);
        System.out.println(name);
        Assert.assertEquals(Methods.getVariable("FinancingLeadClass"), leadFinancialClass);

        leads.switchLeadStatus(driver, leads.testLead, leads.financingToggleNo);
        methods.clickElement(driver, leads.becomeProToggleYes);
        methods.waitMethMS(200);
        methods.clickElement(driver, leads.closeLeadModal);
        List<WebElement> goldenLeadsList = driver.findElements(leads.allLeadsConcat);
        String leadBecomeProClass = goldenLeadsList.get(0).getAttribute("class");
        Assert.assertEquals(Methods.getVariable("BecomeProClass"), leadBecomeProClass);

        leads.switchLeadStatus(driver, leads.testLead, leads.financingToggleYes);
        methods.waitMethMS(200);
        methods.clickElement(driver, leads.closeLeadModal);
        List<WebElement> proAndFinancingList = driver.findElements(leads.allLeadsConcat);
        String becomeProAndFinancing = proAndFinancingList.get(0).getAttribute("class");
        Assert.assertEquals(Methods.getVariable("BecomeProAndFinancing"), becomeProAndFinancing);

        methods.clickElement(driver, leads.testLead);
        methods.clickElement(driver, leads.highPriorityBtn);
        methods.waitMethMS(200);
        methods.clickElement(driver, leads.closeLeadModal);
        List<WebElement> proFinancingPriority = driver.findElements(leads.allLeadsConcat);
        String becomeProFinancingPriority = proFinancingPriority.get(0).getAttribute("class");
        Assert.assertEquals(Methods.getVariable("BecomeProFinancingPriority"), becomeProFinancingPriority);
        leads.switchLeadStatus(driver, leads.testLead, leads.financingToggleNo);
        methods.clickElement(driver, leads.becomeProToggleNo);
        methods.clickElement(driver, leads.highPriorityBtn);
    }

    @Test
    public void leadCalcHistory() throws Exception {
        driver.get(Methods.getVariable("CardealURL"));
        clientPage.moveTooltipToStorage(driver);
        clientPage.getLotInsideUSCars(driver, clientPage.lotsList);
        methods.waitForElementIsVisible(clientPage.vinNumber, 20);
        String vinNumber = methods.getElementText(driver, clientPage.vinNumber);
        driver.get(Methods.getVariable("AdminPanelURL"));
        leads.login(Methods.getVariable("Platform"), Methods.getVariable("AdminLogin"), Methods.getVariable("AdminPassword"));
        methods.clickElement(driver, leads.testLead);
        methods.clickElement(driver, leads.calculatorTab);
        Boolean isAddANewCarClickable = methods.isElementClickable(leads.addNewCarBtn);
        Assert.assertTrue(isAddANewCarClickable);
        Boolean isLeftScrollClickable = methods.isElementClickable(leads.scrollLeftBtn);
        Assert.assertFalse(isLeftScrollClickable);
        Boolean isRightScrollClickable = methods.isElementClickable(leads.scrollRight);
        Assert.assertFalse(isRightScrollClickable);
        methods.waitForElementIsVisible(leads.addLotByVINFld, 20);
        methods.inputValue(driver, vinNumber, leads.addLotByVINFld);
        methods.waitForElementIsClickable(leads.addACarBtn, 20);
        methods.clickElement(driver, leads.addACarBtn);
        methods.waitMethMS(500);
        String actualVin = methods.getElementText(driver, leads.vinCodeCalculator);
        Assert.assertEquals(vinNumber, actualVin);

        Boolean isRightScrollClickableNow = methods.isElementClickable(leads.scrollRight);
        Assert.assertTrue(isRightScrollClickableNow);
        methods.clickElement(driver, leads.scrollRight);
        Boolean isLeftScrollClickableNow = methods.isElementClickable(leads.scrollLeftBtn);
        Assert.assertTrue(isLeftScrollClickableNow);
        methods.clickElement(driver, leads.scrollLeftBtn);
        methods.waitMethMS(500);
        methods.clickElement(driver, leads.closeLotInfoBlock);
        methods.waitMethMS(500);
    }

    @Test
    public void leadsOrderBy() throws Exception {
        String code = "+995 Georgia (საქართველო)";
        leads.login(Methods.getVariable("Platform"), Methods.getVariable("AdminLogin"), Methods.getVariable("AdminPassword"));
        methods.clickElement(driver, leads.createLeadBtn);
        methods.waitMethMS(500);
        methods.inputValue(driver, Methods.getVariable("UserFullName"), leads.userFullNameFld);
        methods.clickElement(driver, leads.countryCodesBtn);
        Methods.chooseItemFromTheList(driver, leads.countryCodesLst, code);
        methods.inputValue(driver, Methods.getVariable("UserPhoneNoCode"), leads.phoneNumberFld);
        methods.clickElement(driver, leads.createNewLeadBtn);
        methods.clickElement(driver, leads.closeLeadModal);
        methods.clickElement(driver, leads.sortByBtn);
        String pastFirst = leads.chooseFilter(driver, leads.orderByDropdown, leads.orderByList, 1);
        Assert.assertEquals(pastFirst, "Past requests first");
        methods.waitForAllElementsAreVisible(leads.regularLeadsList, 20);
        methods.waitMethMS(1000);
        List<WebElement> leadsListPast = driver.findElements(leads.allLeadsConcat);
        String leadName = leadsListPast.get(leadsListPast.size() - 1).getText();
        System.out.println(leadName);
        Assert.assertTrue(leadName.contains(Methods.getVariable("UserFullName")));

        leads.resetFilters(driver);
        methods.clickElement(driver, leads.sortByBtn);
        String recentFirst = leads.chooseFilter(driver, leads.orderByDropdown, leads.orderByList, 0);
        Assert.assertEquals(recentFirst, "Recent requests first");
        methods.waitForAllElementsAreVisible(leads.regularLeadsList, 20);
        methods.waitMethMS(1000);
        List<WebElement> leadsListRecent = driver.findElements(leads.allLeadsConcat);
        String recentLeadName = leadsListRecent.get(0).getText();
        System.out.println(recentLeadName);
        Assert.assertTrue(leadName.contains(Methods.getVariable("UserFullName")));

        leads.resetFilters(driver);
        methods.clickElement(driver, leads.sortByBtn);
        String highPriorityFirst = leads.chooseFilter(driver, leads.orderByDropdown, leads.orderByList, 2);
        Assert.assertEquals(highPriorityFirst, "Prioritized first");
        methods.waitForAllElementsAreVisible(leads.allLeadsExceptRegular, 20);
        methods.waitMethMS(1000);
        List<WebElement> highPriorityFirstList = driver.findElements(leads.allLeadsConcat);
        String highPriorityLead = highPriorityFirstList.get(0).getAttribute("class");
        System.out.println(highPriorityLead);
        Assert.assertTrue(highPriorityLead.contains("high-priority lead-item"));

        leads.resetFilters(driver);
        methods.clickElement(driver, leads.sortByBtn);
        String notPriorFirst = leads.chooseFilter(driver, leads.orderByDropdown, leads.orderByList, 3);
        Assert.assertEquals(notPriorFirst, "Unprioritized first");
        methods.waitForAllElementsAreVisible(leads.regularLeadsList, 20);
        methods.waitMethMS(1000);
        List<WebElement> unprioritizedFirstList = driver.findElements(leads.allLeadsConcat);
        String unprioritizedLead = unprioritizedFirstList.get(0).getAttribute("class");
        System.out.println(unprioritizedLead);
        Assert.assertEquals(unprioritizedLead, "lead-item");
    }

    @Test
    public void leadsSortByChannel() throws Exception {
        String phone = "Phone";
        String telegram = "Telegram";
        String website = "Website";
        List expectedChannelItems = new ArrayList();
        Collections.addAll(expectedChannelItems, "Phone", "Email", "Website", "Facebook Messenger", "Carfax", "Telegram", "Facebook", "Rajdeba");
        leads.login(Methods.getVariable("Platform"), Methods.getVariable("AdminLogin"), Methods.getVariable("AdminPassword"));
        methods.clickElement(driver, leads.sortByBtn);
        methods.clickElement(driver, leads.byChannelDropdown);
        methods.waitForAllElementsAreVisible(leads.byChannelList, 20);
        ArrayList<String> actualChannelItem = Methods.getItemsList(driver, leads.byChannelList);
        Assert.assertEquals(expectedChannelItems, actualChannelItem);

        leads.chooseFilter(driver, leads.byChannelList, telegram);
        ArrayList<String> telegramChannel = Methods.getItemsList(driver, leads.channelTelegram, "src");
        Assert.assertThat(telegramChannel, Every.everyItem(Matchers.containsString("telegram")));

        leads.resetFilters(driver);
        methods.clickElement(driver, leads.sortByBtn);
        leads.chooseFilter(driver, leads.byChannelDropdown, leads.byChannelList, phone);
        ArrayList<String> phoneChannel = Methods.getItemsList(driver, leads.channelPhone, "src");
        Assert.assertThat(phoneChannel, Every.everyItem(Matchers.containsString("phone")));

        leads.resetFilters(driver);
        methods.clickElement(driver, leads.sortByBtn);
        leads.chooseFilter(driver, leads.byChannelDropdown, leads.byChannelList, website);
        methods.waitMethMS(500);
        ArrayList<String> websiteChannel = Methods.getItemsList(driver, leads.channelWebSite, "class");
        Assert.assertThat(websiteChannel, Every.everyItem(Matchers.containsString("channel-icon__icon ui-icon ui-icon_link")));
    }

    @Test
    public void userGroups() throws Exception {
        leads.login(Methods.getVariable("Platform"), Methods.getVariable("AdminLogin"), Methods.getVariable("AdminPassword"));
        methods.clickElement(driver, leads.sortByBtn);
        leads.chooseUserGroup(driver, leads.verifiedCheckbox);
        methods.waitMethMS(500);
        ArrayList<String> verifiedUser = Methods.getItemsList(driver, leads.leadsVerification, "class");
        System.out.println(verifiedUser);
        Assert.assertThat(verifiedUser, Every.everyItem(Matchers.containsString("type_onboard")));

        leads.resetFilters(driver);
        methods.clickElement(driver, leads.sortByBtn);
        leads.chooseUserGroup(driver, leads.unverifiedCheckbox);
        methods.waitMethMS(500);
        ArrayList<String> unverifiedUser = Methods.getItemsList(driver, leads.leadsVerification, "class");
        System.out.println(unverifiedUser);
        Assert.assertThat(unverifiedUser, Every.everyItem(Matchers.containsString("type_guest")));

        leads.resetFilters(driver);
        methods.clickElement(driver, leads.sortByBtn);
        leads.chooseUserGroup(driver, leads.prioritizedCheckbox);
        methods.waitMethMS(500);
        ArrayList<String> prioritizedLeadsList = Methods.getItemsList(driver, leads.allLeads, "class");
        System.out.println(prioritizedLeadsList);
        Assert.assertThat(prioritizedLeadsList, Every.everyItem(Matchers.containsString("high-priority")));

        leads.resetFilters(driver);
        methods.clickElement(driver, leads.sortByBtn);
        leads.chooseUserGroup(driver, leads.financedCheckbox);
        methods.waitMethMS(500);
        ArrayList<String> financedLeadsList = Methods.getItemsList(driver, leads.allLeads, "class");
        System.out.println(financedLeadsList);
        Assert.assertThat(financedLeadsList, Every.everyItem(Matchers.containsString("financing")));

        leads.resetFilters(driver);
        methods.clickElement(driver, leads.sortByBtn);
        leads.chooseUserGroup(driver, leads.proCandidateCheckbox);
        methods.waitMethMS(500);
        ArrayList<String> proLeadsList = Methods.getItemsList(driver, leads.allLeads, "class");
        System.out.println(proLeadsList);
        Assert.assertThat(proLeadsList, Every.everyItem(Matchers.containsString("interested-in-pro")));
    }

    @Test
    public void upperSearch() throws Exception {
        leads.login(Methods.getVariable("Platform"), Methods.getVariable("AdminLogin"), Methods.getVariable("AdminPassword"));
        methods.inputValue(driver, Methods.getVariable("UserEmail"), leads.upperSearchField);
        methods.waitForElementIsVisible(leads.quickSearch, 20);
        String quickSearchName = methods.getElementText(driver, leads.quickSearchName);
        String quickSearchPhone = methods.getElementText(driver, leads.quickSearchPhone);
        Assert.assertEquals(Methods.getVariable("UserFullName"), quickSearchName);
        Assert.assertEquals(Methods.getVariable("UserPhone"), quickSearchPhone);

        methods.clickElement(driver, leads.quickSearch);
        String userName = methods.getElementText(driver, leads.leadHeaderUserName);
        Boolean leadsDetailsModal = methods.isElementPresent(leads.leadHeaderUserName);
        Assert.assertTrue(leadsDetailsModal && userName.contains(Methods.getVariable("UserFullName")));

        methods.clickElement(driver, leads.closeLeadModal);
        methods.clickElement(driver, leads.searchClearBtn);
        methods.inputValue(driver, Methods.getVariable("UserPhoneNoSpaces"), leads.upperSearchField);
        methods.waitForElementIsVisible(leads.quickSearch, 20);
        String quickSearchNameByPhone = methods.getElementText(driver, leads.quickSearchName);
        String quickSearchPhoneByPhone = methods.getElementText(driver, leads.quickSearchPhone);
        Assert.assertEquals(Methods.getVariable("UserFullName"), quickSearchNameByPhone);
        Assert.assertEquals(Methods.getVariable("UserPhone"), quickSearchPhoneByPhone);

        methods.clickElement(driver, leads.quickSearch);
        String userNameModalByPhone = methods.getElementText(driver, leads.leadHeaderUserName);
        Boolean leadsDetailsModalByPhone = methods.isElementPresent(leads.leadHeaderUserName);
        Assert.assertTrue(leadsDetailsModalByPhone && userNameModalByPhone.contains(Methods.getVariable("UserFullName")));
        methods.clickElement(driver, leads.closeLeadModal);
        methods.clickElement(driver, leads.searchClearBtn);
        methods.inputValue(driver, Methods.getVariable("UserFullName"), leads.upperSearchField);
        methods.waitForElementIsVisible(leads.quickSearch, 20);
        String quickSearchNameByName = methods.getElementText(driver, leads.quickSearchName);
        String quickSearchPhoneByName = methods.getElementText(driver, leads.quickSearchPhone);
        Assert.assertEquals(Methods.getVariable("UserFullName"), quickSearchNameByName);
        Assert.assertEquals(Methods.getVariable("UserPhone"), quickSearchPhoneByName);
        methods.clickElement(driver, leads.quickSearch);
        String userNameModalByName = methods.getElementText(driver, leads.leadHeaderUserName);
        Boolean leadsDetailsModalByName = methods.isElementPresent(leads.leadHeaderUserName);
        Assert.assertTrue(leadsDetailsModalByName && userNameModalByName.contains(Methods.getVariable("UserFullName")));
    }

    @Test
    public void lowerSearch() throws Exception {
        leads.login(Methods.getVariable("Platform"), Methods.getVariable("AdminLogin"), Methods.getVariable("AdminPassword"));
        methods.jsClick(leads.lowerSearchBtn);
        methods.waitForElementIsClickable(leads.lowerSearchFld, 10);
        methods.inputValue(driver, Methods.getVariable("UserEmail"), leads.lowerSearchFld);
        methods.waitForNumberOfElementsToBe(leads.allLeads, 1, 20);
        List<WebElement> leadsByEmail = driver.findElements(leads.allLeads);
        Assert.assertTrue(leadsByEmail.size() == 1);

        methods.clickElement(driver, leads.allLeads);
        String userNameByEmail = methods.getElementText(driver, leads.leadHeaderUserName);
        System.out.println(userNameByEmail);
        Assert.assertEquals(Methods.getVariable("UserFullName"), userNameByEmail);
        methods.clickElement(driver, leads.closeLeadModal);
        methods.clickElement(driver, leads.closeLowerSearch);
        methods.jsClick(leads.lowerSearchBtn);
        methods.waitForElementIsClickable(leads.lowerSearchFld, 10);
        methods.inputValue(driver, Methods.getVariable("UserPhoneNoSpaces"), leads.lowerSearchFld);
        methods.waitForNumberOfElementsToBe(leads.allLeads, 1, 20);
        List<WebElement> leadsByPhone = driver.findElements(leads.allLeads);
        Assert.assertTrue(leadsByPhone.size() == 1);

        methods.clickElement(driver, leads.allLeads);
        String userNameByPhone = methods.getElementText(driver, leads.leadHeaderUserName);
        Assert.assertEquals(Methods.getVariable("UserFullName"), userNameByPhone);
        methods.clickElement(driver, leads.closeLeadModal);
        methods.clickElement(driver, leads.closeLowerSearch);
        methods.jsClick(leads.lowerSearchBtn);
        methods.waitForElementIsClickable(leads.lowerSearchFld, 10);
        methods.inputValue(driver, Methods.getVariable("UserFullName"), leads.lowerSearchFld);
        methods.waitForNumberOfElementsToBe(leads.allLeads, 1, 20);
        List<WebElement> leadsByName = driver.findElements(leads.allLeads);
        Assert.assertTrue(leadsByName.size() == 1);
        methods.clickElement(driver, leads.allLeads);
        String userNameByName = methods.getElementText(driver, leads.leadHeaderUserName);
        Assert.assertEquals(Methods.getVariable("UserFullName"), userNameByName);
    }

    @Test
    public void leadStatusTabs() throws Exception {
        List expectedStatuses = new ArrayList();
        Collections.addAll(expectedStatuses, "Waiting for call", "On-hold", "Looking for a car", "Offer Sent");
        leads.login(Methods.getVariable("Platform"), Methods.getVariable("AdminLogin"), Methods.getVariable("AdminPassword"));
        methods.clickElement(driver, leads.testLead);
        methods.clickElement(driver, leads.changeLeadsStatusDropdown);
        ArrayList<String> actualStatuses = Methods.getItemsList(driver, leads.changeLeadsStatusList);
        Assert.assertEquals(expectedStatuses, actualStatuses);

        String onHold = Methods.chooseItemFromTheList(driver, leads.changeLeadsStatusList, 1);
        leads.closeDueDateAndLead(driver, leads.dueDateDoneBtn, leads.closeLeadModal);
        methods.clickElement(driver, leads.onHoldTab);
        Methods.waitForElementInTheListText(driver, Methods.getVariable("UserFullName"), leads.allLeadsByName);
        String onHoldStatus = methods.getElementText(driver, leads.currentLeadStatus);
        String[] onHoldStatusSplit = onHoldStatus.split("\\s+");
        String onHoldStatusFinal = onHoldStatusSplit[0];
        Assert.assertEquals(onHold, onHoldStatusFinal);

        methods.clickElement(driver, leads.changeLeadsStatusDropdown);
        String lookingForACar = Methods.chooseItemFromTheList(driver, leads.changeLeadsStatusList, 2);
        leads.closeDueDateAndLead(driver, leads.dueDateDoneBtn, leads.closeLeadModal);
        methods.clickElement(driver, leads.lookingForACarTab);
        Methods.waitForElementInTheListText(driver, Methods.getVariable("UserFullName"), leads.allLeadsByName);
        String lookingForACarStatus = methods.getElementText(driver, leads.currentLeadStatus);
        String[] lookingForACarStatusSplit = lookingForACarStatus.split("\\s+");
        String lookingForACarStatusFinal = lookingForACarStatusSplit[0];
        Assert.assertTrue(lookingForACar.contains(lookingForACarStatusFinal));

        methods.clickElement(driver, leads.changeLeadsStatusDropdown);
        String offerSent = Methods.chooseItemFromTheList(driver, leads.changeLeadsStatusList, 3);
        methods.clickElement(driver, leads.closeLeadModal);
        methods.clickElement(driver, leads.offerSent);
        Methods.waitForElementInTheListText(driver, Methods.getVariable("UserFullName"), leads.allLeadsByName);
        String offerSentStatus = methods.getElementText(driver, leads.currentLeadStatus);
        String[] offerSentStatusSplit = offerSentStatus.split("\\s+");
        String offerSentStatusFinal = offerSentStatusSplit[0];
        Assert.assertTrue(offerSent.contains(offerSentStatusFinal));

        methods.clickElement(driver, leads.changeLeadsStatusDropdown);
        String waitingForCall = Methods.chooseItemFromTheList(driver, leads.changeLeadsStatusList, 0);
        methods.clickElement(driver, leads.closeLeadModal);
        methods.clickElement(driver, leads.waitingForCallTab);
        Methods.waitForElementInTheListText(driver, Methods.getVariable("UserFullName"), leads.allLeadsByName);
        String waitingForCallStatus = methods.getElementText(driver, leads.currentLeadStatus);
        String[] waitingForCallStatusSplit = waitingForCallStatus.split("\\s+");
        String waitingForCallStatusFinal = waitingForCallStatusSplit[0];
        Assert.assertTrue(waitingForCall.contains(waitingForCallStatusFinal));
    }

    @Test
    public void LeadStatusAssigned() throws Exception {
        leads.login(Methods.getVariable("Platform"), Methods.getVariable("AdminLogin"), Methods.getVariable("AdminPassword"));
        Methods.chooseItemFromTheList(driver, leads.allLeadsByName, Methods.getVariable("UserFullName"));
        methods.clickElement(driver, leads.assignToDropdownBtn);
        methods.waitForAllElementsAreVisible(leads.brokersList, 10);
        Methods.chooseItemFromTheList(driver, leads.brokersList, Methods.getVariable("BrokerFullName"));
        methods.clickElement(driver, leads.closeLeadModal);
        methods.clickElement(driver, leads.assignedTab);
        Methods.waitForElementInTheListText(driver, Methods.getVariable("UserFullName"), leads.allLeadsByName);
        String currentBrokerName = methods.getElementText(driver, leads.currentBrokerName);
        Assert.assertEquals(Methods.getVariable("BrokerFullName"), currentBrokerName);

        methods.clickElement(driver, leads.changeLeadsStatusDropdown);
        String onHold = Methods.chooseItemFromTheList(driver, leads.changeLeadsStatusList, 1);
        leads.closeDueDateAndLead(driver, leads.dueDateDoneBtn, leads.closeLeadModal);
        methods.clickElement(driver, leads.onHoldTab);
        methods.waitForElementIsClickable(leads.allLeadsByName, 20);
        Methods.waitForElementInTheListText(driver, Methods.getVariable("UserFullName"), leads.allLeadsByName);
        String onHoldStatus = methods.getElementText(driver, leads.currentLeadStatus);
        String[] onHoldStatusSplit = onHoldStatus.split("\\s+");
        String onHoldStatusFinal = onHoldStatusSplit[0];
        Assert.assertEquals(onHold, onHoldStatusFinal);

        methods.clickElement(driver, leads.changeLeadsStatusDropdown);
        String lookingForACar = Methods.chooseItemFromTheList(driver, leads.changeLeadsStatusList, 2);
        leads.closeDueDateAndLead(driver, leads.dueDateDoneBtn, leads.closeLeadModal);
        methods.clickElement(driver, leads.lookingForACarTab);
        methods.waitForElementIsClickable(leads.allLeadsByName, 20);
        Methods.waitForElementInTheListText(driver, Methods.getVariable("UserFullName"), leads.allLeadsByName);
        String lookingForACarStatus = methods.getElementText(driver, leads.currentLeadStatus);
        String[] lookingForACarStatusSplit = lookingForACarStatus.split("\\s+");
        String lookingForACarStatusFinal = lookingForACarStatusSplit[0];
        Assert.assertTrue(lookingForACar.contains(lookingForACarStatusFinal));

        methods.clickElement(driver, leads.changeLeadsStatusDropdown);
        String offerSent = Methods.chooseItemFromTheList(driver, leads.changeLeadsStatusList, 3);
        methods.clickElement(driver, leads.closeLeadModal);
        methods.clickElement(driver, leads.offerSent);
        methods.waitForElementIsClickable(leads.allLeadsByName, 20);
        Methods.waitForElementInTheListText(driver, Methods.getVariable("UserFullName"), leads.allLeadsByName);
        String offerSentStatus = methods.getElementText(driver, leads.currentLeadStatus);
        String[] offerSentStatusSplit = offerSentStatus.split("\\s+");
        String offerSentStatusFinal = offerSentStatusSplit[0];
        Assert.assertTrue(offerSent.contains(offerSentStatusFinal));

        methods.clickElement(driver, leads.changeLeadsStatusDropdown);
        String waitingForCall = Methods.chooseItemFromTheList(driver, leads.changeLeadsStatusList, 0);
        methods.clickElement(driver, leads.closeLeadModal);
        methods.clickElement(driver, leads.waitingForCallTab);
        methods.waitForElementIsClickable(leads.allLeadsByName, 20);
        Methods.waitForElementInTheListText(driver, Methods.getVariable("UserFullName"), leads.allLeadsByName);
        String waitingForCallStatus = methods.getElementText(driver, leads.currentLeadStatus);
        String[] waitingForCallStatusSplit = waitingForCallStatus.split("\\s+");
        String waitingForCallStatusFinal = waitingForCallStatusSplit[0];
        Assert.assertTrue(waitingForCall.contains(waitingForCallStatusFinal));

        methods.clickElement(driver, leads.assignToDropdownBtn);
        methods.waitForAllElementsAreVisible(leads.brokersList, 10);
        Methods.chooseItemFromTheList(driver, leads.brokersList, "Unassign");
        methods.clickElement(driver, leads.closeLeadModal);
        methods.clickElement(driver, leads.unassignedTab);
        methods.waitForElementIsClickable(leads.allLeadsByName, 20);

        Methods.waitForElementInTheListText(driver, Methods.getVariable("UserFullName"), leads.allLeadsByName);
        String currentBrokerNameLast = methods.getElementText(driver, leads.currentBrokerName);
        Assert.assertEquals("Assign to", currentBrokerNameLast);
    }

    @Test
    public void sortingByBroker() throws Exception {
        leads.login(Methods.getVariable("Platform"), Methods.getVariable("AdminLogin"), Methods.getVariable("AdminPassword"));
        Methods.chooseItemFromTheList(driver, leads.allLeadsByName, Methods.getVariable("UserFullName"));
        methods.clickElement(driver, leads.assignToDropdownBtn);
        methods.waitForAllElementsAreVisible(leads.brokersList, 10);
        Methods.chooseItemFromTheList(driver, leads.brokersList, Methods.getVariable("BrokerFullName"));
        methods.clickElement(driver, leads.closeLeadModal);
        methods.clickElement(driver, leads.assignedTab);
        methods.clickElement(driver, leads.sortByBtn);
        methods.clickElement(driver, leads.byBroker);
        methods.waitForAllElementsAreVisible(leads.byBrokerList, 20);
        Methods.chooseItemFromTheList(driver, leads.byBrokerList, Methods.getVariable("BrokerFullName"));
        methods.clickElement(driver, leads.setOptionsBtn);
        methods.waitMethMS(500);
//        try {
            methods.waitForAllElementsAreVisible(leads.allLeadsByBroker, 10);
//        } catch (StaleElementReferenceException e) {
//
//        }
        ArrayList<String> leadsList = Methods.getItemsList(driver, leads.allLeadsByBroker);
        System.out.println(leadsList);
        Assert.assertThat(leadsList, Every.everyItem(Matchers.containsString(Methods.getVariable("BrokerFullName"))));

        Methods.chooseItemFromTheList(driver, leads.allLeadsByName, Methods.getVariable("UserFullName"));
        methods.clickElement(driver, leads.assignToDropdownBtn);
        methods.waitForAllElementsAreVisible(leads.brokersList, 10);
        Methods.chooseItemFromTheList(driver, leads.brokersList, "Unassign");
    }

    
//    @After
//    public void shutDown() {
//        driver.quit();
//    }

}
