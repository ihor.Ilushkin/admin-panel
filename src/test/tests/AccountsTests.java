import org.hamcrest.Matchers;
import org.hamcrest.core.Every;
import org.junit.*;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

public class AccountsTests {

    WebDriver driver;
    Methods methods;
    Leads leads;
    AccountsPage accountsPage;

    @Rule
    public ScreenshotRule screenshotRule = new ScreenshotRule();

    @Before
    public void setUp() throws Exception {
        driver = new ChromeDriver();
        screenshotRule.setDriver(driver);

        driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
        driver.manage().timeouts().pageLoadTimeout(60, TimeUnit.SECONDS);
        driver.manage().window().maximize();
        driver.get(Methods.getVariable("AdminPanelURL"));
        methods = new Methods(driver);
        leads = new Leads(driver);
        accountsPage = new AccountsPage(driver);
    }

    @Test
    public void sortByBidLimit() throws Exception {
        String min = "200";
        String max = "1000";
        int minInt = Integer.parseInt(min);
        int maxInt = Integer.parseInt(max);
        leads.login(Methods.getVariable("Platform"), Methods.getVariable("AdminLogin"), Methods.getVariable("AdminPassword"));
        accountsPage.openAccountSubTab();
        accountsPage.sortByBidLimit(min, max);
        methods.waitForAllElementsAreVisible(accountsPage.bidLimitsList, 20);
        ArrayList<Integer> currentLimits = AccountsPage.convertStringListToInt(driver, accountsPage.bidLimitsList);
        for (int i = 0; i < currentLimits.size(); i++) {
            int bid = currentLimits.get(i);
            Assert.assertTrue(bid >= minInt && bid <= maxInt);
        }
    }

    @Test
    public void sortByInvalidCredentials() throws Exception {
        leads.login(Methods.getVariable("Platform"), Methods.getVariable("AdminLogin"), Methods.getVariable("AdminPassword"));
        accountsPage.openAccountSubTab();
        methods.clickElement(driver, accountsPage.sortByButton);
        methods.clickElement(driver, accountsPage.invalidCredentialsCheckbox);
        methods.clickElement(driver, accountsPage.setOptionsButton);
        methods.waitForElementIsNotVisible(accountsPage.loading, 20);
        ArrayList<String> statusList = Methods.getItemsList(driver, accountsPage.statusList);
        Assert.assertThat(statusList, Every.everyItem(Matchers.equalTo("Invalid credentials")));
    }

    @Test
    public void setBidLimit() throws Exception {
        String bidLimit = "10000";
        int intBidLimit = Integer.parseInt(bidLimit);
        leads.login(Methods.getVariable("Platform"), Methods.getVariable("AdminLogin"), Methods.getVariable("AdminPassword"));
        accountsPage.openAccountSubTab();
        methods.clickElement(driver, accountsPage.bidLimitButton);
        methods.clearFieldFastLoop(driver, accountsPage.bidLimitField);
        methods.inputValue(driver, bidLimit, accountsPage.bidLimitField);
        methods.clickElement(driver, accountsPage.bidLimitApplyButton);
        methods.waitForElementIsNotVisible(accountsPage.loading, 20);
        ArrayList<String> upperBidLimit = Methods.getItemsList(driver, accountsPage.bidLimitsList);
        int currentBidLimit = Integer.parseInt(upperBidLimit.get(0).replace("$", "")
                .replace(",", ""));
        Assert.assertEquals(intBidLimit, currentBidLimit);

        methods.clickElement(driver, accountsPage.bidLimitButton);
        methods.clearFieldFastLoop(driver, accountsPage.bidLimitField);
        methods.clickElement(driver, accountsPage.bidLimitApplyButton);
        methods.waitForElementIsNotVisible(accountsPage.loading, 20);
        ArrayList<String> notSetArr = Methods.getItemsList(driver, accountsPage.bidLimitsList);
        String notSet = notSetArr.get(0);
        Assert.assertEquals(notSet, "Not set");
    }

    @Test
    public void copartAccountManagement() throws Exception {
        leads.login(Methods.getVariable("Platform"), Methods.getVariable("AdminLogin"), Methods.getVariable("AdminPassword"));
        accountsPage.openAccountSubTab();

        // Dealers & Vehicles links check
        methods.waitForAllElementsAreVisible(accountsPage.dealersList, 20);
        methods.clickElement(driver, accountsPage.dealersList);
        methods.waitForUrlContains("dealers?copartLogin", 20);
        Assert.assertTrue(driver.getCurrentUrl().contains("dealers?copartLogin"));
        driver.navigate().back();
        methods.waitForAllElementsAreVisible(accountsPage.vehiclesList, 20);
        methods.clickElement(driver, accountsPage.vehiclesList);
        methods.waitForUrlContains("won?copartLogin", 20);
        Assert.assertTrue(driver.getCurrentUrl().contains("won?copartLogin"));
        accountsPage.openAccountSubTab();

        // Change password
        methods.waitForAllElementsAreVisible(accountsPage.copartAccountManageButtonsList, 20);
        List<WebElement> manageButtonsList = driver.findElements(accountsPage.copartAccountManageButtonsList);
        manageButtonsList.get(0).click();
        accountsPage.changePassword(Methods.getVariable("Password"), Methods.getVariable("Password"));
        String passwordNotification = methods.getElementText(driver, accountsPage.notificationText);
        Assert.assertTrue(passwordNotification.contains("Password changed for account"));
        methods.clickElement(driver, accountsPage.closeNotification);
        ArrayList<String> statusList = Methods.getItemsList(driver, accountsPage.statusList);
        if (statusList.get(0).contains("Blocked")) {
            manageButtonsList.get(0).click();
            methods.clickElement(driver, accountsPage.blockAccountButton);
        }
        methods.waitForTextToBePresentedInElement(accountsPage.statusList, "Active", 20);
        ArrayList<String> statusListUpd = Methods.getItemsList(driver, accountsPage.statusList);
        Assert.assertTrue(statusListUpd.get(0).contains("Active"));

        // Block unblock account
        methods.clickElement(driver, accountsPage.closeNotification);
        List<WebElement> blockButton = driver.findElements(accountsPage.blockAccountButton);
        if (blockButton.size() == 0) {
            methods.clickElement(driver, accountsPage.copartAccountManageButtonsList);
        }
        methods.clickElement(driver, accountsPage.blockAccountButton);
        methods.waitForElementIsVisible(accountsPage.notificationText, 20);
        String blockAccountNotification = methods.getElementText(driver, accountsPage.notificationText);
        String blockStatus = methods.getElementText(driver, accountsPage.statusList);
        Assert.assertTrue(blockAccountNotification.contains("blocked") && blockStatus.contains("Blocked"));
        methods.clickElement(driver, accountsPage.closeNotification);
        methods.clickElement(driver, accountsPage.copartAccountManageButtonsList);
        methods.clickElement(driver, accountsPage.blockAccountButton);
        methods.waitForElementIsVisible(accountsPage.notificationText, 20);
        methods.waitForTextToBePresentedInElement(accountsPage.statusList, "Active", 20);
        String unblockedStatusNotification = methods.getElementText(driver, accountsPage.notificationText);
        String unblockStatus = methods.getElementText(driver, accountsPage.statusList);
        Assert.assertTrue(unblockedStatusNotification.contains("unblocked") && unblockStatus.contains("Active"));

        // Enable/Disable ios
        List iosEnabledBefore = Methods.getItemsList(driver, accountsPage.iosPicture);
        int picAmount = iosEnabledBefore.size();
        methods.clickElement(driver, accountsPage.enableIOSButton);
        methods.waitForNumberOfElementsToBe(accountsPage.iosPicture, picAmount + 1, 20);
        List iosEnabledAfter = Methods.getItemsList(driver, accountsPage.iosPicture);
        Assert.assertTrue(iosEnabledAfter.size() == iosEnabledBefore.size() + 1);
        methods.clickElement(driver, accountsPage.enableIOSButton);
        methods.waitForNumberOfElementsToBe(accountsPage.iosPicture, picAmount, 20);
        List iosEnabledAfterAll = Methods.getItemsList(driver, accountsPage.iosPicture);
        Assert.assertTrue(iosEnabledAfterAll.size() == iosEnabledBefore.size());
    }


}
