import junit.framework.JUnit4TestAdapter;
import org.junit.*;
import org.junit.internal.runners.statements.Fail;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

import java.awt.*;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;

public class UsersTests {

    WebDriver driver;
    Methods methods;
    Leads leads;
    ClientPage clientPage;
    UsersPage usersPage;

    @Rule
    public ScreenshotRule screenshotRule = new ScreenshotRule();


    @Before
    public void setUp() throws Exception {
        driver = new ChromeDriver();
        screenshotRule.setDriver(driver);
        driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
        driver.manage().timeouts().pageLoadTimeout(60, TimeUnit.SECONDS);
        driver.manage().window().maximize();
        driver.get(Methods.getVariable("AdminPanelURL"));
        methods = new Methods(driver);
        leads = new Leads(driver);
        clientPage = new ClientPage(driver);
        usersPage = new UsersPage(driver);

    }

    @Test
    public void successfulRegularUserCreation() throws Exception {
        String code = "+995 Georgia (საქართველო)";
        leads.login(Methods.getVariable("Platform"), Methods.getVariable("AdminLogin"), Methods.getVariable("AdminPassword"));
        methods.clickElement(driver, usersPage.usersTab);
        methods.clickElement(driver, usersPage.newUserBtn);
        String user = usersPage.chooseUserType(driver, usersPage.accountTypeDropdown, usersPage.accountTypesList, 0);
        System.out.println(user);
        methods.clickElement(driver, usersPage.codesDropdown);
        Methods.chooseItemFromTheList(driver, usersPage.codesList, code);
        String name = methods.randomNameLastName(6);
        String lastName = methods.randomNameLastName(6);
        String email = methods.randomEmail();
        String phone = usersPage.randomPhone();
        usersPage.userCreation(driver, name, lastName, email, phone, "password", "password");
        methods.clickElement(driver, usersPage.createUserBtn);
        methods.waitForElementIsNotVisible(usersPage.userCreateModalHeader, 20);
        ArrayList<String> names = Methods.getItemsList(driver, usersPage.usersListByName);
        String firstNameInTheList = names.get(0);
        Assert.assertTrue(firstNameInTheList.contains(name) && firstNameInTheList.contains(lastName));

        ArrayList<String> phones = Methods.getItemsList(driver, usersPage.userListByPhone);
        String firstPhoneInTheList = phones.get(0);
        Assert.assertTrue(firstPhoneInTheList.contains(phone));

        ArrayList<String> emails = Methods.getItemsList(driver, usersPage.userListByEmail);
        String firstEmailInTheList = emails.get(0);
        Assert.assertEquals(firstEmailInTheList, email);

        driver.get(Methods.getVariable("CardealURL"));
        clientPage.login(driver, clientPage.profileBtn, clientPage.emailPhoneFld, email, clientPage.passwordFld, Methods.getVariable("Password"), clientPage.logInBtn);
        methods.clickElement(driver, clientPage.profileBtn);
        String userName = methods.getElementText(driver, clientPage.rightSideBarHeader);
        System.out.println(userName);
        Assert.assertEquals(userName, name + " " + lastName);

        driver.get(Methods.getVariable("AdminPanelURL"));
        methods.clickElement(driver, usersPage.usersTab);
        Methods.waitForElementInTheListText(driver, name, usersPage.usersListByName, usersPage.manageUserCredButtonsList);
        methods.clickElement(driver, usersPage.deleteAccBtn);
        methods.clickElement(driver, usersPage.confirmUserDeletionBtn);
        methods.waitMethMS(500);
        ArrayList<String> usersList = Methods.getItemsList(driver, usersPage.usersListByName);
        String absenceCheck = usersList.get(0);
        System.out.println(absenceCheck);
        Assert.assertFalse(absenceCheck.contains(name));
    }

    @Test
    public void successfulProUserCreation() throws Exception {
        String code = "+995 Georgia (საქართველო)";
        leads.login(Methods.getVariable("Platform"), Methods.getVariable("AdminLogin"), Methods.getVariable("AdminPassword"));
        methods.clickElement(driver, usersPage.usersTab);
        methods.clickElement(driver, usersPage.newUserBtn);
        usersPage.chooseUserType(driver, usersPage.accountTypeDropdown, usersPage.accountTypesList, 1);
        methods.clickElement(driver, usersPage.codesDropdown);
        Methods.chooseItemFromTheList(driver, usersPage.codesList, code);
        String name = methods.randomNameLastName(6);
        String lastName = methods.randomNameLastName(6);
        String email = methods.randomEmail();
        String phone = usersPage.randomPhone();
        usersPage.userCreation(driver, name, lastName, email, phone, "password", "password");
        methods.clickElement(driver, usersPage.createUserBtn);
        methods.waitForElementIsNotVisible(usersPage.userCreateModalHeader, 20);
        methods.clickElement(driver, usersPage.proUsersHeaderTab);
        methods.waitMethMS(500);
        String tabName = methods.getElementText(driver, usersPage.proUsersHeaderTab);
        Assert.assertEquals("PRO-USERS", tabName);
        ArrayList<String> names = Methods.getItemsList(driver, usersPage.usersListByName);
        String firstNameInTheList = names.get(0);
        System.out.println(firstNameInTheList);
        Assert.assertTrue(firstNameInTheList.contains(name) && firstNameInTheList.contains(lastName));

        ArrayList<String> phones = Methods.getItemsList(driver, usersPage.userListByPhone);
        String firstPhoneInTheList = phones.get(0);
        Assert.assertTrue(firstPhoneInTheList.contains(phone));

        ArrayList<String> emails = Methods.getItemsList(driver, usersPage.userListByEmail);
        String firstEmailInTheList = emails.get(0);
        Assert.assertEquals(firstEmailInTheList, email);

        driver.get(Methods.getVariable("CardealURL"));
        clientPage.login(driver, clientPage.profileBtn, clientPage.emailPhoneFld, email, clientPage.passwordFld, Methods.getVariable("Password"), clientPage.logInBtn);
        methods.clickElement(driver, clientPage.profileBtn);
        String userName = methods.getElementText(driver, clientPage.rightSideBarHeader);
        System.out.println(userName);
        Assert.assertEquals(userName, name + " " + lastName);

        driver.get(Methods.getVariable("AdminPanelURL"));
        methods.clickElement(driver, usersPage.usersTab);
        methods.clickElement(driver, usersPage.proUsersHeaderTab);
        methods.waitMethMS(500);
        Methods.waitForElementInTheListText(driver, name, usersPage.usersListByName, usersPage.manageUserCredButtonsList);
        methods.clickElement(driver, usersPage.deleteAccBtn);
        methods.clickElement(driver, usersPage.confirmUserDeletionBtn);
        methods.waitMethMS(500);
        ArrayList<String> usersList = Methods.getItemsList(driver, usersPage.usersListByName);
        String absenceCheck = usersList.get(0);
        System.out.println(absenceCheck);
        Assert.assertFalse(absenceCheck.contains(name));
    }

    @Test
    public void successfulBrokerCreation() throws Exception {
        String code = "+995 Georgia (საქართველო)";
        leads.login(Methods.getVariable("Platform"), Methods.getVariable("AdminLogin"), Methods.getVariable("AdminPassword"));
        methods.clickElement(driver, usersPage.usersTab);
        methods.clickElement(driver, usersPage.newUserBtn);
        usersPage.chooseUserType(driver, usersPage.accountTypeDropdown, usersPage.accountTypesList, 2);
        methods.clickElement(driver, usersPage.codesDropdown);
        Methods.chooseItemFromTheList(driver, usersPage.codesList, code);
        String name = methods.randomNameLastName(6);
        String lastName = methods.randomNameLastName(6);
        String email = methods.randomEmail();
        String phone = usersPage.randomPhone();
        usersPage.userCreation(driver, name, lastName, email, phone, "password", "password");
        methods.clickElement(driver, usersPage.createUserBtn);
        methods.waitForElementIsNotVisible(usersPage.userCreateModalHeader, 20);
        methods.clickElement(driver, usersPage.brokersHeaderTab);
        methods.waitMethMS(500);
        String tabName = methods.getElementText(driver, usersPage.brokersHeaderTab);
        Assert.assertEquals("BROKERS", tabName);
        ArrayList<String> names = Methods.getItemsList(driver, usersPage.usersListByName);
        String firstNameInTheList = names.get(0);
        System.out.println(firstNameInTheList);
        Assert.assertTrue(firstNameInTheList.contains(name) && firstNameInTheList.contains(lastName));

        ArrayList<String> phones = Methods.getItemsList(driver, usersPage.userListByPhone);
        String firstPhoneInTheList = phones.get(0);
        Assert.assertTrue(firstPhoneInTheList.contains(phone));

        ArrayList<String> emails = Methods.getItemsList(driver, usersPage.userListByEmail);
        String firstEmailInTheList = emails.get(0);
        Assert.assertEquals(firstEmailInTheList, email);

        driver.get(Methods.getVariable("CardealURL"));
        clientPage.login(driver, clientPage.profileBtn, clientPage.emailPhoneFld, email, clientPage.passwordFld, Methods.getVariable("Password"), clientPage.logInBtn);
        methods.clickElement(driver, clientPage.profileBtn);
        String userName = methods.getElementText(driver, clientPage.rightSideBarHeader);
        System.out.println(userName);
        Assert.assertEquals(userName, name + " " + lastName);

        driver.get(Methods.getVariable("AdminPanelURL"));
        methods.clickElement(driver, usersPage.usersTab);
        methods.clickElement(driver, usersPage.brokersHeaderTab);
        methods.waitMethMS(500);
        Methods.waitForElementInTheListText(driver, name, usersPage.usersListByName, usersPage.manageUserCredButtonsList);
        methods.clickElement(driver, usersPage.deleteAccBtn);
        methods.clickElement(driver, usersPage.confirmUserDeletionBtn);
        methods.waitMethMS(500);
        ArrayList<String> usersList = Methods.getItemsList(driver, usersPage.usersListByName);
        String absenceCheck = usersList.get(0);
        System.out.println(absenceCheck);
        Assert.assertFalse(absenceCheck.contains(name));
    }

    @Test
    public void successfulDealerCreation() throws Exception {
        String code = "+995 Georgia (საქართველო)";
        leads.login(Methods.getVariable("Platform"), Methods.getVariable("AdminLogin"), Methods.getVariable("AdminPassword"));
        methods.clickElement(driver, usersPage.usersTab);
        methods.clickElement(driver, usersPage.newUserBtn);
        usersPage.chooseUserType(driver, usersPage.accountTypeDropdown, usersPage.accountTypesList, 3);
        methods.clickElement(driver, usersPage.codesDropdown);
        Methods.chooseItemFromTheList(driver, usersPage.codesList, code);
        String name = methods.randomNameLastName(6);
        String lastName = methods.randomNameLastName(6);
        String email = methods.randomEmail();
        String phone = usersPage.randomPhone();
        usersPage.userCreation(driver, name, lastName, email, phone, "password", "password");
        methods.clickElement(driver, usersPage.createUserBtn);
        methods.waitForElementIsNotVisible(usersPage.userCreateModalHeader, 20);
        methods.clickElement(driver, usersPage.dealersHeaderTab);
        methods.waitMethMS(500);
        String tabName = methods.getElementText(driver, usersPage.dealersHeaderTab);
        Assert.assertEquals("DEALERS", tabName);
        ArrayList<String> names = Methods.getItemsList(driver, usersPage.usersListByName);
        String firstNameInTheList = names.get(0);
        System.out.println(firstNameInTheList);
        Assert.assertTrue(firstNameInTheList.contains(name) && firstNameInTheList.contains(lastName));

        ArrayList<String> phones = Methods.getItemsList(driver, usersPage.userListByPhone);
        String firstPhoneInTheList = phones.get(0);
        Assert.assertTrue(firstPhoneInTheList.contains(phone));

        ArrayList<String> emails = Methods.getItemsList(driver, usersPage.userListByEmail);
        String firstEmailInTheList = emails.get(0);
        Assert.assertEquals(firstEmailInTheList, email);

        driver.get(Methods.getVariable("CardealURL"));
        clientPage.login(driver, clientPage.profileBtn, clientPage.emailPhoneFld, email, clientPage.passwordFld, Methods.getVariable("Password"), clientPage.logInBtn);
        methods.clickElement(driver, clientPage.profileBtn);
        String userName = methods.getElementText(driver, clientPage.rightSideBarHeader);
        System.out.println(userName);
        Assert.assertEquals(userName, name + " " + lastName);

        driver.get(Methods.getVariable("AdminPanelURL"));
        methods.clickElement(driver, usersPage.usersTab);
        methods.clickElement(driver, usersPage.dealersHeaderTab);
        methods.waitMethMS(500);
        Methods.waitForElementInTheListText(driver, name, usersPage.usersListByName, usersPage.manageUserCredButtonsList);
        methods.clickElement(driver, usersPage.deleteAccBtn);
        methods.clickElement(driver, usersPage.confirmUserDeletionBtn);
        methods.waitMethMS(500);
        ArrayList<String> usersList = Methods.getItemsList(driver, usersPage.usersListByName);
        String absenceCheck = usersList.get(0);
        System.out.println(absenceCheck);
        Assert.assertFalse(absenceCheck.contains(name));
    }

    @Test
    public void userCreationWarnings() throws Exception {
        leads.login(Methods.getVariable("Platform"), Methods.getVariable("AdminLogin"), Methods.getVariable("AdminPassword"));
        methods.clickElement(driver, usersPage.usersTab);
        methods.clickElement(driver, usersPage.newUserBtn);
        usersPage.userCreation(driver, "", methods.randomNameLastName(6), methods.randomEmail(), methods.randomPhone(), "password", "password");
        methods.clickElement(driver, usersPage.createUserBtn);
        String nameWarn = methods.getElementText(driver, usersPage.warnings);
        Assert.assertEquals("The field is required", nameWarn);

        methods.inputValue(driver, methods.randomNameLastName(6), usersPage.firstNameFld);
        methods.clearFieldLoop(driver, usersPage.lastNameFld);
        methods.clickElement(driver, usersPage.createUserBtn);
        String lastNameWarn = methods.getElementText(driver, usersPage.warnings);
        Assert.assertEquals("The field is required", lastNameWarn);

        methods.inputValue(driver, methods.randomNameLastName(6), usersPage.lastNameFld);
        methods.clearFieldLoop(driver, usersPage.emailFld);
        methods.clickElement(driver, usersPage.createUserBtn);
        String emptyEmailWarn = methods.getElementText(driver, usersPage.warnings);
        Assert.assertEquals("The field is required", emptyEmailWarn);

        methods.inputValue(driver, "asadadad.com", usersPage.emailFld);
        methods.clickElement(driver, usersPage.createUserBtn);//
        String invalidEmailWarn = methods.getCustomAttribute(driver, usersPage.emailFld, "validationMessage");
        Assert.assertTrue(invalidEmailWarn.contains("Please include an '@' in the email address"));

        methods.clearFieldLoop(driver, usersPage.emailFld);
        methods.inputValue(driver, methods.randomEmail(), usersPage.emailFld);
        methods.clearFieldLoop(driver, usersPage.phoneField);
        methods.clickElement(driver, usersPage.createUserBtn);
        String emptyPhoneWarn = methods.getElementText(driver, usersPage.phoneFlwWarnings);
        Assert.assertEquals("The field is required", emptyPhoneWarn);

        methods.inputValue(driver, "999999", usersPage.phoneField);
        methods.clickElement(driver, usersPage.createUserBtn);
        String invalidPhoneWarn = methods.getElementText(driver, usersPage.phoneFlwWarnings);
        Assert.assertEquals("The value should be a valid phone number", invalidPhoneWarn);

        methods.clearFieldLoop(driver, usersPage.phoneField);
        methods.inputValue(driver, methods.randomPhone(), usersPage.phoneField);
        methods.clearFieldLoop(driver, usersPage.passwordFld);
        methods.clickElement(driver, usersPage.createUserBtn);
        String emptyPassWarn = methods.getElementText(driver, usersPage.warnings);
        Assert.assertEquals("The field is required", emptyPassWarn);

        methods.inputValue(driver, Methods.getVariable("ShortPassword"), usersPage.passwordFld);
        methods.clearFieldLoop(driver, usersPage.rePasswordFld);
        methods.inputValue(driver, Methods.getVariable("ShortPassword"), usersPage.rePasswordFld);
        methods.clickElement(driver, usersPage.createUserBtn);
        String shortPassWarn = methods.getElementText(driver, usersPage.warnings);
        Assert.assertEquals("The password should be min 8 chars and max 64 chars length", shortPassWarn);

        methods.clearFieldLoop(driver, usersPage.passwordFld);
        methods.inputValue(driver, Methods.getVariable("LongPassword"), usersPage.passwordFld);
        methods.inputValue(driver, Methods.getVariable("LongPassword"), usersPage.rePasswordFld);
        methods.clickElement(driver, usersPage.createUserBtn);
        String longPassWarn = methods.getElementText(driver, usersPage.warnings);
        Assert.assertEquals("The password should be min 8 chars and max 64 chars length", longPassWarn);

        methods.clearFieldLoop(driver, usersPage.passwordFld);
        methods.clearFieldLoop(driver, usersPage.rePasswordFld);
        methods.inputValue(driver, "password", usersPage.passwordFld);
        methods.inputValue(driver, "passwor", usersPage.passwordFld);
        methods.clickElement(driver, usersPage.createUserBtn);
        String mismatchWarn = methods.getElementText(driver, usersPage.warnings);
        Assert.assertEquals("The passwords are mismatched", mismatchWarn);
    }

    @Test
    public void userPromotion() throws Exception { //todo: try to reduce registration code
        String code = "+995 Georgia (საქართველო)";
        leads.login(Methods.getVariable("Platform"), Methods.getVariable("AdminLogin"), Methods.getVariable("AdminPassword"));
        methods.clickElement(driver, usersPage.usersTab);
        methods.clickElement(driver, usersPage.newUserBtn);
        usersPage.chooseUserType(driver, usersPage.accountTypeDropdown, usersPage.accountTypesList, 0);
        methods.clickElement(driver, usersPage.codesDropdown);
        Methods.chooseItemFromTheList(driver, usersPage.codesList, code);
        String name = methods.randomNameLastName(6);
        String lastName = methods.randomNameLastName(6);
        String email = methods.randomEmail();
        String phone = usersPage.randomPhone();
        usersPage.userCreation(driver, name, lastName, email, phone, "password", "password");
        methods.clickElement(driver, usersPage.createUserBtn);
        methods.waitForElementIsNotVisible(usersPage.userCreateModalHeader, 20);
        List<WebElement> regularUsersList = driver.findElements(usersPage.usersListByName);
        String regular = regularUsersList.get(0).getText();
        Assert.assertTrue(regular.contains(name));
        Assert.assertTrue(driver.getCurrentUrl().contains("regular"));

        Methods.chooseItemFromTheList(driver, usersPage.usersListByName, name);
        String userLbl = methods.getElementText(driver, usersPage.userTypeLbl);
        Assert.assertEquals("User", userLbl);
        usersPage.chooseUserType(driver, usersPage.createdUserModalTypeDropdown, usersPage.accountTypesList, 1);
        List<WebElement> proUsersList = driver.findElements(usersPage.usersListByName);
        String pro = proUsersList.get(0).getText();
        methods.waitForUrlContains("pro", 10);
        Assert.assertTrue(driver.getCurrentUrl().contains("pro"));
        Assert.assertTrue(pro.contains(name));

        methods.waitMethMS(500);
        Methods.chooseItemFromTheList(driver, usersPage.usersListByName, name);
        String proLbl = methods.getElementText(driver, usersPage.userTypeLbl);
        Assert.assertEquals("Pro-user", proLbl);
        usersPage.chooseUserType(driver, usersPage.createdUserModalTypeDropdown, usersPage.accountTypesList, 2);
        List<WebElement> brokersList = driver.findElements(usersPage.usersListByName);
        String broker = brokersList.get(0).getText();
        methods.waitForUrlContains("brokers", 10);
        Assert.assertTrue(driver.getCurrentUrl().contains("brokers"));
        Assert.assertTrue(broker.contains(name));

        methods.waitMethMS(500);
        Methods.chooseItemFromTheList(driver, usersPage.usersListByName, name);
        String brokerLbl = methods.getElementText(driver, usersPage.userTypeLbl);
        Assert.assertEquals("Broker", brokerLbl);
        methods.clickElement(driver, usersPage.closeUserCreationModal);
        Methods.waitForElementInTheListText(driver, name, usersPage.usersListByName, usersPage.manageUserCredButtonsList);
        methods.clickElement(driver, usersPage.deleteAccBtn);
        methods.clickElement(driver, usersPage.confirmUserDeletionBtn);

    }

    @Test
    public void sortBy() throws Exception {
        String code = "+995 Georgia (საქართველო)";
        leads.login(Methods.getVariable("Platform"), Methods.getVariable("AdminLogin"), Methods.getVariable("AdminPassword"));
        methods.clickElement(driver, usersPage.usersTab);
        // Regular user
        methods.clickElement(driver, usersPage.newUserBtn);
        usersPage.chooseUserType(driver, usersPage.accountTypeDropdown, usersPage.accountTypesList, 0);
        methods.clickElement(driver, usersPage.codesDropdown);
        Methods.chooseItemFromTheList(driver, usersPage.codesList, code);
        usersPage.userCreation(driver, methods.randomNameLastName(6), methods.randomNameLastName(6), methods.randomEmail(), methods.randomPhone(), "password", "password");
        methods.clickElement(driver, usersPage.createUserBtn);
        methods.waitForElementIsNotVisible(usersPage.userCreateModalHeader, 20);
        String newLast = usersPage.sortBy(driver, usersPage.sortByBtn, usersPage.orderByDropdown, usersPage.orderByList, 1, usersPage.setOptionBtn);
        Assert.assertEquals("New last", newLast);
        methods.waitForElementIsNotVisible(usersPage.orderByModal, 10);
        ArrayList<String> regularCreatedLast = Methods.getItemsList(driver, usersPage.createdAtList);
        String regularLast = regularCreatedLast.get(0);
        Assert.assertTrue(regularLast.contains("year"));
        String newFirst = usersPage.sortBy(driver, usersPage.sortByBtn, usersPage.orderByDropdown, usersPage.orderByList, 0, usersPage.setOptionBtn);
        Assert.assertEquals("New first", newFirst);
        methods.waitForElementIsNotVisible(usersPage.orderByModal, 10);
        ArrayList<String> regularCreatedFirst = Methods.getItemsList(driver, usersPage.createdAtList);
        String regularFirst = regularCreatedFirst.get(0);
        Assert.assertTrue(regularFirst.contains("seconds") || regularFirst.contains("now"));
        usersPage.deleteUser(usersPage.manageUserCredButtonsList, usersPage.deleteAccBtn, usersPage.confirmUserDeletionBtn);

        //Pro user
        methods.clickElement(driver, usersPage.proUsersHeaderTab);
        methods.clickElement(driver, usersPage.newUserBtn);
        usersPage.chooseUserType(driver, usersPage.accountTypeDropdown, usersPage.accountTypesList, 1);
        methods.clickElement(driver, usersPage.codesDropdown);
        Methods.chooseItemFromTheList(driver, usersPage.codesList, code);
        usersPage.userCreation(driver, methods.randomNameLastName(6), methods.randomNameLastName(6), methods.randomEmail(), methods.randomPhone(), "password", "password");
        methods.clickElement(driver, usersPage.createUserBtn);
        methods.waitForElementIsNotVisible(usersPage.userCreateModalHeader, 20);
        String newProLast = usersPage.sortBy(driver, usersPage.sortByBtn, usersPage.orderByDropdown, usersPage.orderByList, 1, usersPage.setOptionBtn);
        Assert.assertEquals("New last", newProLast);
        methods.waitForElementIsNotVisible(usersPage.orderByModal, 10);
        ArrayList<String> proCreatedLast = Methods.getItemsList(driver, usersPage.createdAtList);
        String proLast = proCreatedLast.get(0);
        Assert.assertTrue(proLast.contains("year"));
        String newProFirst = usersPage.sortBy(driver, usersPage.sortByBtn, usersPage.orderByDropdown, usersPage.orderByList, 0, usersPage.setOptionBtn);
        Assert.assertEquals("New first", newProFirst);
        methods.waitForElementIsNotVisible(usersPage.orderByModal, 10);
        ArrayList<String> proCreatedFirst = Methods.getItemsList(driver, usersPage.createdAtList);
        String proFirst = proCreatedFirst.get(0);
        Assert.assertTrue(proFirst.contains("seconds") || proFirst.contains("now"));
        usersPage.deleteUser(usersPage.manageUserCredButtonsList, usersPage.deleteAccBtn, usersPage.confirmUserDeletionBtn);

        //Brokers
        methods.clickElement(driver, usersPage.brokersHeaderTab);
        methods.clickElement(driver, usersPage.newUserBtn);
        usersPage.chooseUserType(driver, usersPage.accountTypeDropdown, usersPage.accountTypesList, 2);
        methods.clickElement(driver, usersPage.codesDropdown);
        Methods.chooseItemFromTheList(driver, usersPage.codesList, code);
        usersPage.userCreation(driver, methods.randomNameLastName(6), methods.randomNameLastName(6), methods.randomEmail(), methods.randomPhone(), "password", "password");
        methods.clickElement(driver, usersPage.createUserBtn);
        methods.waitForElementIsNotVisible(usersPage.userCreateModalHeader, 20);
        String newBrokerLast = usersPage.sortBy(driver, usersPage.sortByBtn, usersPage.orderByDropdown, usersPage.orderByList, 1, usersPage.setOptionBtn);
        Assert.assertEquals("New last", newBrokerLast);
        methods.waitForElementIsNotVisible(usersPage.orderByModal, 10);
        ArrayList<String> brokerCreatedLast = Methods.getItemsList(driver, usersPage.createdAtList);
        String brokerLast = brokerCreatedLast.get(0);
        Assert.assertTrue(brokerLast.contains("year"));
        String newBrokerFirst = usersPage.sortBy(driver, usersPage.sortByBtn, usersPage.orderByDropdown, usersPage.orderByList, 0, usersPage.setOptionBtn);
        Assert.assertEquals("New first", newBrokerFirst);
        methods.waitForElementIsNotVisible(usersPage.orderByModal, 10);
        ArrayList<String> brokerCreatedFirst = Methods.getItemsList(driver, usersPage.createdAtList);
        String brokerFirst = brokerCreatedFirst.get(0);
        Assert.assertTrue(brokerFirst.contains("seconds") || brokerFirst.contains("now"));
        usersPage.deleteUser(usersPage.manageUserCredButtonsList, usersPage.deleteAccBtn, usersPage.confirmUserDeletionBtn);

        //Dealers
        methods.clickElement(driver, usersPage.dealersHeaderTab);
        methods.clickElement(driver, usersPage.newUserBtn);
        usersPage.chooseUserType(driver, usersPage.accountTypeDropdown, usersPage.accountTypesList, 3);
        methods.clickElement(driver, usersPage.codesDropdown);
        Methods.chooseItemFromTheList(driver, usersPage.codesList, code);
        usersPage.userCreation(driver, methods.randomNameLastName(6), methods.randomNameLastName(6), methods.randomEmail(), methods.randomPhone(), "password", "password");
        methods.clickElement(driver, usersPage.createUserBtn);
        methods.waitForElementIsNotVisible(usersPage.userCreateModalHeader, 20);
        String newDealerLast = usersPage.sortBy(driver, usersPage.sortByBtn, usersPage.orderByDropdown, usersPage.orderByList, 1, usersPage.setOptionBtn);
        Assert.assertEquals("New last", newDealerLast);
        methods.waitForElementIsNotVisible(usersPage.orderByModal, 10);
        ArrayList<String> dealerCreatedLast = Methods.getItemsList(driver, usersPage.createdAtList);
        String dealerLast = dealerCreatedLast.get(0);
        Assert.assertTrue(dealerLast.contains("year"));
        String newDealerFirst = usersPage.sortBy(driver, usersPage.sortByBtn, usersPage.orderByDropdown, usersPage.orderByList, 0, usersPage.setOptionBtn);
        Assert.assertEquals("New first", newDealerFirst);
        methods.waitForElementIsNotVisible(usersPage.orderByModal, 10);
        ArrayList<String> dealerCreatedFirst = Methods.getItemsList(driver, usersPage.createdAtList);
        String dealerFirst = dealerCreatedFirst.get(0);
        Assert.assertTrue(dealerFirst.contains("seconds") || dealerFirst.contains("now"));
        usersPage.deleteUser(usersPage.manageUserCredButtonsList, usersPage.deleteAccBtn, usersPage.confirmUserDeletionBtn);
    }

    @Test
    public void usersSearch() throws Exception {
        leads.login(Methods.getVariable("Platform"), Methods.getVariable("AdminLogin"), Methods.getVariable("AdminPassword"));
        methods.clickElement(driver, usersPage.usersTab);
        // Regular users search
        usersPage.usersSearch(Methods.getVariable("UserFullName"));
        methods.waitForNumberOfElementsLessThen(usersPage.usersListByName, 2, 20);
        ArrayList<String> userByName = Methods.getItemsList(driver, usersPage.usersListByName);
        String userName = userByName.get(0);
        Assert.assertEquals(Methods.getVariable("UserFullName"), userName);

        methods.clickElement(driver, usersPage.clearSearchBtn);
        methods.waitForNumberOfElementsMoreThan(usersPage.userListByPhone, 5, 20);
        usersPage.usersSearch(Methods.getVariable("UserPhone"));
        methods.waitForNumberOfElementsLessThen(usersPage.userListByPhone, 2, 20);
        ArrayList<String> userByPhone = Methods.getItemsList(driver, usersPage.userListByPhone);
        String userPhone = userByPhone.get(0);
        Assert.assertEquals(Methods.getVariable("UserPhone"), userPhone);

        methods.clickElement(driver, usersPage.clearSearchBtn);
        methods.waitForNumberOfElementsMoreThan(usersPage.userListByEmail, 5, 20);
        usersPage.usersSearch(Methods.getVariable("UserEmail"));
        methods.waitForNumberOfElementsLessThen(usersPage.userListByPhone, 2, 20);
        ArrayList<String> userByEmail = Methods.getItemsList(driver, usersPage.userListByEmail);
        String userEmail = userByEmail.get(0);
        Assert.assertEquals(Methods.getVariable("UserEmail"), userEmail);

        // Pro users search
        methods.clickElement(driver, usersPage.clearSearchBtn);
        methods.clickElement(driver, usersPage.proUsersHeaderTab);
        usersPage.usersSearch(Methods.getVariable("ProUserFullName"));
        methods.waitForNumberOfElementsLessThen(usersPage.usersListByName, 2, 20);
        ArrayList<String> proUserByName = Methods.getItemsList(driver, usersPage.usersListByName);
        String proUserName = proUserByName.get(0);
        Assert.assertEquals(Methods.getVariable("ProUserFullName"), proUserName);

        methods.clickElement(driver, usersPage.clearSearchBtn);
        methods.waitForNumberOfElementsMoreThan(usersPage.userListByPhone, 5, 20);
        usersPage.usersSearch(Methods.getVariable("ProUserPhone"));
        methods.waitForNumberOfElementsLessThen(usersPage.userListByPhone, 2, 20);
        ArrayList<String> proUserByPhone = Methods.getItemsList(driver, usersPage.userListByPhone);
        String proUserPhone = proUserByPhone.get(0);
        Assert.assertEquals(Methods.getVariable("ProUserPhone"), proUserPhone);

        methods.clickElement(driver, usersPage.clearSearchBtn);
        methods.waitForNumberOfElementsMoreThan(usersPage.userListByEmail, 5, 20);
        usersPage.usersSearch(Methods.getVariable("ProUserEmail"));
        methods.waitForNumberOfElementsLessThen(usersPage.userListByEmail, 2, 20);
        ArrayList<String> proUserByEmail = Methods.getItemsList(driver, usersPage.userListByEmail);
        String proUserEmail = proUserByEmail.get(0);
        Assert.assertEquals(Methods.getVariable("ProUserEmail"), proUserEmail);

        // Brokers search
        methods.clickElement(driver, usersPage.clearSearchBtn);
        methods.clickElement(driver, usersPage.brokersHeaderTab);
        usersPage.usersSearch(Methods.getVariable("BrokerFullName"));
        methods.waitForNumberOfElementsLessThen(usersPage.usersListByName, 2, 20);
        ArrayList<String> brokerByName = Methods.getItemsList(driver, usersPage.usersListByName);
        String brokerName = brokerByName.get(0);
        Assert.assertEquals(Methods.getVariable("BrokerFullName"), brokerName);

        methods.clickElement(driver, usersPage.clearSearchBtn);
        methods.waitForNumberOfElementsMoreThan(usersPage.userListByPhone, 5, 20);
        usersPage.usersSearch(Methods.getVariable("BrokerPhone"));
        methods.waitForNumberOfElementsLessThen(usersPage.userListByPhone, 2, 20);
        ArrayList<String> brokerByPhone = Methods.getItemsList(driver, usersPage.userListByPhone);
        String brokerPhone = brokerByPhone.get(0);
        Assert.assertEquals(Methods.getVariable("BrokerPhone"), brokerPhone);

        methods.clickElement(driver, usersPage.clearSearchBtn);
        methods.waitForNumberOfElementsMoreThan(usersPage.userListByEmail, 5, 20);
        usersPage.usersSearch(Methods.getVariable("BrokerEmail"));
        methods.waitForNumberOfElementsLessThen(usersPage.userListByEmail, 2, 20);
        ArrayList<String> brokerByEmail = Methods.getItemsList(driver, usersPage.userListByEmail);
        String brokerEmail = brokerByEmail.get(0);
        Assert.assertEquals(Methods.getVariable("BrokerEmail"), brokerEmail);

        // Dealers search
        methods.clickElement(driver, usersPage.clearSearchBtn);
        methods.clickElement(driver, usersPage.dealersHeaderTab);
        usersPage.usersSearch(Methods.getVariable("DealerFullName"));
        methods.waitForNumberOfElementsLessThen(usersPage.usersListByName, 2, 20);
        ArrayList<String> dealerByName = Methods.getItemsList(driver, usersPage.usersListByName);
        String dealerName = dealerByName.get(0);
        Assert.assertEquals(Methods.getVariable("DealerFullName"), dealerName);

        methods.clickElement(driver, usersPage.clearSearchBtn);
        methods.waitForNumberOfElementsMoreThan(usersPage.userListByPhone, 5, 20);
        usersPage.usersSearch(Methods.getVariable("DealerPhoneNumber"));
        methods.waitForNumberOfElementsLessThen(usersPage.userListByPhone, 2, 20);
        ArrayList<String> dealerByPhone = Methods.getItemsList(driver, usersPage.userListByPhone);
        String dealerPhone = dealerByPhone.get(0);
        Assert.assertEquals(Methods.getVariable("DealerPhoneNumber"), dealerPhone);

        methods.clickElement(driver, usersPage.clearSearchBtn);
        methods.waitForNumberOfElementsMoreThan(usersPage.userListByEmail, 5, 20);
        usersPage.usersSearch(Methods.getVariable("DealerEmail"));
        methods.waitForNumberOfElementsLessThen(usersPage.userListByEmail, 2, 20);
        ArrayList<String> dealerByEmail = Methods.getItemsList(driver, usersPage.userListByEmail);
        String dealerEmail = dealerByEmail.get(0);
        Assert.assertEquals(Methods.getVariable("DealerEmail"), dealerEmail);

    }

    @Test
    public void userManageCredentials() throws Exception {
        String code = "+995 Georgia (საქართველო)";
        leads.login(Methods.getVariable("Platform"), Methods.getVariable("AdminLogin"), Methods.getVariable("AdminPassword"));
        methods.clickElement(driver, usersPage.usersTab);
        ArrayList<String> userData = usersPage.fullUsersCreation(driver, 0, code);
        String userName = userData.get(0);
        String userEmail = userData.get(2);
        System.out.println(userName);
        Methods.waitForElementInTheListText(driver, userName, usersPage.usersListByName, usersPage.manageUserCredButtonsList);
        methods.clickElement(driver, usersPage.verifyPhoneLink);
        methods.waitForElementIsVisible(usersPage.userManagingNotificationsText, 10);
        String phoneVerification = methods.getElementText(driver, usersPage.userManagingNotificationsText);
        Assert.assertTrue(phoneVerification.contains("Verified phone"));

        methods.clickElement(driver, usersPage.closeNotificationBtn);
        methods.waitForElementIsNotVisible(usersPage.closeNotificationBtn, 10);
        Methods.waitForElementInTheListText(driver, userName, usersPage.usersListByName, usersPage.manageUserCredButtonsList);
        methods.clickElement(driver, usersPage.verifyEmailLink);
        methods.waitForElementIsVisible(usersPage.userManagingNotificationsText, 10);
        String emailVerification = methods.getElementText(driver, usersPage.userManagingNotificationsText);
        Assert.assertTrue(emailVerification.contains("Verified email"));

        usersPage.changeUserPassword(Methods.getVariable("NewPassword"), Methods.getVariable("NewPassword"));
        driver.get(Methods.getVariable("CardealURL"));
        // Login with old password
        clientPage.login(driver, clientPage.profileBtn, clientPage.emailPhoneFld, userEmail, clientPage.passwordFld, Methods.getVariable("Password"), clientPage.logInBtn);
        String loginWarn = methods.getElementText(driver, clientPage.loginWarningMessage);
        Assert.assertEquals("Invalid login or password", loginWarn);
        clientPage.clearLoginAndPass(driver);
        // Login with new password
        clientPage.login(driver, userEmail, Methods.getVariable("NewPassword"));
        boolean isEmailVerifyBtnDisplayed = clientPage.isVerified(driver, clientPage.verifyEmailBtn);
        Assert.assertFalse(isEmailVerifyBtnDisplayed);
        boolean isPhoneVerifyBtnDisplayed = clientPage.isVerified(driver, clientPage.verifyPhoneBtn);
        Assert.assertFalse(isPhoneVerifyBtnDisplayed);

        clientPage.logout();
        driver.get(Methods.getVariable("AdminPanelURL"));
        methods.clickElement(driver, usersPage.usersTab);
        Methods.waitForElementInTheListText(driver, userName, usersPage.usersListByName, usersPage.manageUserCredButtonsList);
        // Block user
        methods.clickElement(driver, usersPage.blockTheUserLink);
        methods.waitForElementIsVisible(usersPage.userManagingNotificationsText, 10);
        String blockMsgText = methods.getElementText(driver, usersPage.userManagingNotificationsText);
        Assert.assertTrue(blockMsgText.contains("blocked"));
        driver.get(Methods.getVariable("CardealURL"));
        clientPage.login(driver, clientPage.profileBtn, clientPage.emailPhoneFld, userEmail, clientPage.passwordFld, Methods.getVariable("NewPassword"), clientPage.logInBtn);
        String blockMsgClientText = methods.getElementText(driver, clientPage.clientWarnings);
        Assert.assertEquals("User blocked. Please contact administrators.", blockMsgClientText);

        // Delete user
        driver.get(Methods.getVariable("AdminPanelURL"));
        methods.clickElement(driver, usersPage.usersTab);
        Methods.waitForElementInTheListText(driver, userName, usersPage.usersListByName, usersPage.manageUserCredButtonsList);
        methods.clickElement(driver, usersPage.deleteAccBtn);
        methods.clickElement(driver, usersPage.confirmUserDeletionBtn);
        methods.waitForElementIsNotVisible(usersPage.confirmUserDeletionBtn, 10);
        ArrayList<String> usersList = Methods.getItemsList(driver, usersPage.usersListByName);
        String user = usersList.get(0);
        Assert.assertFalse(user.contains(userName));
    }

    @Test
    public void proUserManageCredentials() throws Exception {
        String code = "+995 Georgia (საქართველო)";
        leads.login(Methods.getVariable("Platform"), Methods.getVariable("AdminLogin"), Methods.getVariable("AdminPassword"));
        methods.clickElement(driver, usersPage.usersTab);
        ArrayList<String> userData = usersPage.fullUsersCreation(driver, 1, code);
        String userName = userData.get(0);
        String userEmail = userData.get(2);
        System.out.println(userName);
        methods.clickElement(driver, usersPage.proUsersHeaderTab);
        Methods.waitForElementInTheListText(driver, userName, usersPage.usersListByName, usersPage.manageUserCredButtonsList);
        methods.clickElement(driver, usersPage.verifyPhoneLink);
        methods.waitForElementIsVisible(usersPage.userManagingNotificationsText, 10);
        String phoneVerification = methods.getElementText(driver, usersPage.userManagingNotificationsText);
        Assert.assertTrue(phoneVerification.contains("Verified phone"));

        methods.clickElement(driver, usersPage.closeNotificationBtn);
        methods.waitForElementIsNotVisible(usersPage.closeNotificationBtn, 10);
        Methods.waitForElementInTheListText(driver, userName, usersPage.usersListByName, usersPage.manageUserCredButtonsList);
        methods.clickElement(driver, usersPage.verifyEmailLink);
        methods.waitForElementIsVisible(usersPage.userManagingNotificationsText, 10);
        String emailVerification = methods.getElementText(driver, usersPage.userManagingNotificationsText);
        Assert.assertTrue(emailVerification.contains("Verified email"));

        usersPage.changeUserPassword(Methods.getVariable("NewPassword"), Methods.getVariable("NewPassword"));
        driver.get(Methods.getVariable("CardealURL"));
        // Login with old password
        clientPage.login(driver, clientPage.profileBtn, clientPage.emailPhoneFld, userEmail, clientPage.passwordFld, Methods.getVariable("Password"), clientPage.logInBtn);
        String loginWarn = methods.getElementText(driver, clientPage.loginWarningMessage);
        Assert.assertEquals("Invalid login or password", loginWarn);
        clientPage.clearLoginAndPass(driver);
        // Login with new password
        clientPage.login(driver, userEmail, Methods.getVariable("NewPassword"));
        boolean isEmailVerifyBtnDisplayed = clientPage.isVerified(driver, clientPage.verifyEmailBtn);
        Assert.assertFalse(isEmailVerifyBtnDisplayed);
        boolean isPhoneVerifyBtnDisplayed = clientPage.isVerified(driver, clientPage.verifyPhoneBtn);
        Assert.assertFalse(isPhoneVerifyBtnDisplayed);

        clientPage.logout();
        driver.get(Methods.getVariable("AdminPanelURL"));
        methods.clickElement(driver, usersPage.usersTab);
        methods.clickElement(driver, usersPage.proUsersHeaderTab);
        Methods.waitForElementInTheListText(driver, userName, usersPage.usersListByName, usersPage.manageUserCredButtonsList);
        // Block pro user
        methods.clickElement(driver, usersPage.blockTheUserLink);
        methods.waitForElementIsVisible(usersPage.userManagingNotificationsText, 10);
        String blockMsgText = methods.getElementText(driver, usersPage.userManagingNotificationsText);
        Assert.assertTrue(blockMsgText.contains("blocked"));
        driver.get(Methods.getVariable("CardealURL"));
        clientPage.login(driver, clientPage.profileBtn, clientPage.emailPhoneFld, userEmail, clientPage.passwordFld, Methods.getVariable("NewPassword"), clientPage.logInBtn);
        String blockMsgClientText = methods.getElementText(driver, clientPage.clientWarnings);
        Assert.assertEquals("User blocked. Please contact administrators.", blockMsgClientText);

        // Delete pro user
        driver.get(Methods.getVariable("AdminPanelURL"));
        methods.clickElement(driver, usersPage.usersTab);
        methods.clickElement(driver, usersPage.proUsersHeaderTab);
        Methods.waitForElementInTheListText(driver, userName, usersPage.usersListByName, usersPage.manageUserCredButtonsList);
        methods.clickElement(driver, usersPage.deleteAccBtn);
        methods.clickElement(driver, usersPage.confirmUserDeletionBtn);
        methods.waitForElementIsNotVisible(usersPage.confirmUserDeletionBtn, 10);
        ArrayList<String> usersList = Methods.getItemsList(driver, usersPage.usersListByName);
        String user = usersList.get(0);
        Assert.assertFalse(user.contains(userName));
    }

    @Test
    public void brokerManageCredentials() throws Exception {
        String code = "+995 Georgia (საქართველო)";
        leads.login(Methods.getVariable("Platform"), Methods.getVariable("AdminLogin"), Methods.getVariable("AdminPassword"));
        methods.clickElement(driver, usersPage.usersTab);
        ArrayList<String> userData = usersPage.fullUsersCreation(driver, 2, code);
        String userName = userData.get(0);
        String userEmail = userData.get(2);
        System.out.println(userName);
        methods.clickElement(driver, usersPage.brokersHeaderTab);
        Methods.waitForElementInTheListText(driver, userName, usersPage.usersListByName, usersPage.manageUserCredButtonsList);
        methods.clickElement(driver, usersPage.verifyPhoneLink);
        methods.waitForElementIsVisible(usersPage.userManagingNotificationsText, 10);
        String phoneVerification = methods.getElementText(driver, usersPage.userManagingNotificationsText);
        Assert.assertTrue(phoneVerification.contains("Verified phone"));

        methods.clickElement(driver, usersPage.closeNotificationBtn);
        methods.waitForElementIsNotVisible(usersPage.closeNotificationBtn, 10);
        Methods.waitForElementInTheListText(driver, userName, usersPage.usersListByName, usersPage.manageUserCredButtonsList);
        methods.clickElement(driver, usersPage.verifyEmailLink);
        methods.waitForElementIsVisible(usersPage.userManagingNotificationsText, 10);
        String emailVerification = methods.getElementText(driver, usersPage.userManagingNotificationsText);
        Assert.assertTrue(emailVerification.contains("Verified email"));

        usersPage.changeUserPassword(Methods.getVariable("NewPassword"), Methods.getVariable("NewPassword"));
        driver.get(Methods.getVariable("CardealURL"));
        // Login with old password
        clientPage.login(driver, clientPage.profileBtn, clientPage.emailPhoneFld, userEmail, clientPage.passwordFld, Methods.getVariable("Password"), clientPage.logInBtn);
        String loginWarn = methods.getElementText(driver, clientPage.loginWarningMessage);
        Assert.assertEquals("Invalid login or password", loginWarn);
        clientPage.clearLoginAndPass(driver);
        // Login with new password
        clientPage.login(driver, userEmail, Methods.getVariable("NewPassword"));
        boolean isEmailVerifyBtnDisplayed = clientPage.isVerified(driver, clientPage.verifyEmailBtn);
        Assert.assertFalse(isEmailVerifyBtnDisplayed);
        boolean isPhoneVerifyBtnDisplayed = clientPage.isVerified(driver, clientPage.verifyPhoneBtn);
        Assert.assertFalse(isPhoneVerifyBtnDisplayed);

        clientPage.logout();
        driver.get(Methods.getVariable("AdminPanelURL"));
        methods.clickElement(driver, usersPage.usersTab);
        methods.clickElement(driver, usersPage.brokersHeaderTab);
        Methods.waitForElementInTheListText(driver, userName, usersPage.usersListByName, usersPage.manageUserCredButtonsList);
        // Block broker
        methods.clickElement(driver, usersPage.blockTheUserLink);
        methods.waitForElementIsVisible(usersPage.userManagingNotificationsText, 10);
        String blockMsgText = methods.getElementText(driver, usersPage.userManagingNotificationsText);
        Assert.assertTrue(blockMsgText.contains("blocked"));
        driver.get(Methods.getVariable("CardealURL"));
        clientPage.login(driver, clientPage.profileBtn, clientPage.emailPhoneFld, userEmail, clientPage.passwordFld, Methods.getVariable("NewPassword"), clientPage.logInBtn);
        String blockMsgClientText = methods.getElementText(driver, clientPage.clientWarnings);
        Assert.assertEquals("User blocked. Please contact administrators.", blockMsgClientText);

        // Delete broker
        driver.get(Methods.getVariable("AdminPanelURL"));
        methods.clickElement(driver, usersPage.usersTab);
        methods.clickElement(driver, usersPage.brokersHeaderTab);
        Methods.waitForElementInTheListText(driver, userName, usersPage.usersListByName, usersPage.manageUserCredButtonsList);
        methods.clickElement(driver, usersPage.deleteAccBtn);
        methods.clickElement(driver, usersPage.confirmUserDeletionBtn);
        methods.waitForElementIsNotVisible(usersPage.confirmUserDeletionBtn, 10);
        ArrayList<String> usersList = Methods.getItemsList(driver, usersPage.usersListByName);
        String user = usersList.get(0);
        Assert.assertFalse(user.contains(userName));
    }

    @Test
    public void dealerManageCredentials() throws Exception {
        String code = "+995 Georgia (საქართველო)";
        leads.login(Methods.getVariable("Platform"), Methods.getVariable("AdminLogin"), Methods.getVariable("AdminPassword"));
        methods.clickElement(driver, usersPage.usersTab);
        ArrayList<String> userData = usersPage.fullUsersCreation(driver, 3, code);
        String userName = userData.get(0);
        String userEmail = userData.get(2);
        System.out.println(userName);
        methods.clickElement(driver, usersPage.dealersHeaderTab);
        Methods.waitForElementInTheListText(driver, userName, usersPage.usersListByName, usersPage.manageUserCredButtonsList);
        methods.clickElement(driver, usersPage.verifyPhoneLink);
        methods.waitForElementIsVisible(usersPage.userManagingNotificationsText, 10);
        String phoneVerification = methods.getElementText(driver, usersPage.userManagingNotificationsText);
        Assert.assertTrue(phoneVerification.contains("Verified phone"));

        methods.clickElement(driver, usersPage.closeNotificationBtn);
        methods.waitForElementIsNotVisible(usersPage.closeNotificationBtn, 10);
        Methods.waitForElementInTheListText(driver, userName, usersPage.usersListByName, usersPage.manageUserCredButtonsList);
        methods.clickElement(driver, usersPage.verifyEmailLink);
        methods.waitForElementIsVisible(usersPage.userManagingNotificationsText, 10);
        String emailVerification = methods.getElementText(driver, usersPage.userManagingNotificationsText);
        Assert.assertTrue(emailVerification.contains("Verified email"));

        usersPage.changeUserPassword(Methods.getVariable("NewPassword"), Methods.getVariable("NewPassword"));
        driver.get(Methods.getVariable("CardealURL"));
        // Login with old password
        clientPage.login(driver, clientPage.profileBtn, clientPage.emailPhoneFld, userEmail, clientPage.passwordFld, Methods.getVariable("Password"), clientPage.logInBtn);
        String loginWarn = methods.getElementText(driver, clientPage.loginWarningMessage);
        Assert.assertEquals("Invalid login or password", loginWarn);
        clientPage.clearLoginAndPass(driver);
        // Login with new password
        clientPage.login(driver, userEmail, Methods.getVariable("NewPassword"));
        boolean isEmailVerifyBtnDisplayed = clientPage.isVerified(driver, clientPage.verifyEmailBtn);
        Assert.assertFalse(isEmailVerifyBtnDisplayed);
        boolean isPhoneVerifyBtnDisplayed = clientPage.isVerified(driver, clientPage.verifyPhoneBtn);
        Assert.assertFalse(isPhoneVerifyBtnDisplayed);

        clientPage.logout();
        driver.get(Methods.getVariable("AdminPanelURL"));
        methods.clickElement(driver, usersPage.usersTab);
        methods.clickElement(driver, usersPage.dealersHeaderTab);
        Methods.waitForElementInTheListText(driver, userName, usersPage.usersListByName, usersPage.manageUserCredButtonsList);
        // Block dealer
        methods.clickElement(driver, usersPage.blockTheUserLink);
        methods.waitForElementIsVisible(usersPage.userManagingNotificationsText, 10);
        String blockMsgText = methods.getElementText(driver, usersPage.userManagingNotificationsText);
        Assert.assertTrue(blockMsgText.contains("blocked"));
        driver.get(Methods.getVariable("CardealURL"));
        clientPage.login(driver, clientPage.profileBtn, clientPage.emailPhoneFld, userEmail, clientPage.passwordFld, Methods.getVariable("NewPassword"), clientPage.logInBtn);
        String blockMsgClientText = methods.getElementText(driver, clientPage.clientWarnings);
        Assert.assertEquals("User blocked. Please contact administrators.", blockMsgClientText);

        // Delete dealer
        driver.get(Methods.getVariable("AdminPanelURL"));
        methods.clickElement(driver, usersPage.usersTab);
        methods.clickElement(driver, usersPage.dealersHeaderTab);
        Methods.waitForElementInTheListText(driver, userName, usersPage.usersListByName, usersPage.manageUserCredButtonsList);
        methods.clickElement(driver, usersPage.deleteAccBtn);
        methods.clickElement(driver, usersPage.confirmUserDeletionBtn);
        methods.waitForElementIsNotVisible(usersPage.confirmUserDeletionBtn, 10);
        ArrayList<String> usersList = Methods.getItemsList(driver, usersPage.usersListByName);
        String user = usersList.get(0);
        Assert.assertFalse(user.contains(userName));
    }

//    @After
//    public void shutDown() {
//        driver.quit();
//    }

}
// Create user all types, Warnings, User promotions, Sorting, Search, User creds managing