import org.hamcrest.Matchers;
import org.hamcrest.core.Every;
import org.junit.*;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

import java.util.ArrayList;
import java.util.concurrent.TimeUnit;

public class ExtensionDealersTests {

    WebDriver driver;
    Methods methods;
    ClientPage clientPage;
    Leads leads;
    ExtensionDealersPage extensionDealersPage;
    UsersPage usersPage;

    @Rule
    public ScreenshotRule screenshotRule = new ScreenshotRule();


    @Before
    public void setUp() throws Exception {
        driver = new ChromeDriver();
        screenshotRule.setDriver(driver);
        driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
        driver.manage().timeouts().pageLoadTimeout(60, TimeUnit.SECONDS);
        driver.manage().window().maximize();
        driver.get(Methods.getVariable("AdminPanelURL"));
        methods = new Methods(driver);
        clientPage = new ClientPage(driver);
        leads = new Leads(driver);
        extensionDealersPage = new ExtensionDealersPage(driver);
        usersPage = new UsersPage(driver);
    }

    @Test
    public void createNewDealer() throws Exception {
        String code = "+995 Georgia (საქართველო)";
        leads.login(Methods.getVariable("Platform"), Methods.getVariable("AdminLogin"), Methods.getVariable("AdminPassword"));
        extensionDealersPage.openDealersSubTab(driver);
        ArrayList<String> dealersData = extensionDealersPage.fullDealerCreation(driver, 1, code);
        System.out.println(dealersData);
        String dealerName = dealersData.get(0);
        String dealerLastName = dealersData.get(1);
        ArrayList<String> firstUserName = Methods.getItemsList(driver, extensionDealersPage.dealersListByName);
        String currentUserName = firstUserName.get(0);
        Assert.assertEquals(dealerName + " " + dealerLastName, currentUserName);

        String generatedEmail = dealersData.get(2);
        ArrayList<String> emailsList = Methods.getItemsList(driver, extensionDealersPage.dealersListByEmail);
        String currentEmail = emailsList.get(0);
        Assert.assertEquals(generatedEmail, currentEmail);

        String chosenCopartLogin = dealersData.get(4);
        ArrayList<String> copartLoginsList = Methods.getItemsList(driver, extensionDealersPage.dealersListByCopartLogin);
        String currentCopartLogin = copartLoginsList.get(0);
        Assert.assertEquals(chosenCopartLogin, currentCopartLogin);

        driver.get(Methods.getVariable("CardealURL"));
        clientPage.login(driver, clientPage.profileBtn, clientPage.emailPhoneFld, currentEmail, clientPage.passwordFld, Methods.getVariable("Password"), clientPage.logInBtn);
        methods.clickElement(driver, clientPage.profileBtn);
        String userName = methods.getElementText(driver, clientPage.rightSideBarHeader);
        Assert.assertEquals(userName, currentUserName);

        driver.get(Methods.getVariable("AdminPanelURL"));
        extensionDealersPage.openDealersSubTab(driver);
        Methods.waitForElementInTheListText(driver, currentUserName, extensionDealersPage.dealersListByName, extensionDealersPage.dealersManageCredentialsListButtons);
        extensionDealersPage.deleteDealer(driver);
        ArrayList<String> usersList = Methods.getItemsList(driver, extensionDealersPage.dealersListByName);
        String absenceCheck = usersList.get(0);
        System.out.println(absenceCheck);
        Assert.assertFalse(absenceCheck.contains(currentUserName));
    }

    @Test
    public void dealerCreationWarnings() throws Exception {
        String code = "+995 Georgia (საქართველო)";
        leads.login(Methods.getVariable("Platform"), Methods.getVariable("AdminLogin"), Methods.getVariable("AdminPassword"));
        extensionDealersPage.openDealersSubTab(driver);
        methods.waitMethMS(1000);
        methods.clickElement(driver, extensionDealersPage.createNewDealerBtn);
        extensionDealersPage.choosePhoneCode(driver, code);
        usersPage.userCreation(driver, "", methods.randomNameLastName(6), methods.randomEmail(), methods.randomPhone(), "password", "password");
        methods.clickElement(driver, extensionDealersPage.createDealerBtn);
        String nameWarn = methods.getElementText(driver, usersPage.warnings);
        Assert.assertEquals("The field is required", nameWarn);

        methods.inputValue(driver, methods.randomNameLastName(6), usersPage.firstNameFld);
        methods.clearFieldLoop(driver, usersPage.lastNameFld);
        //methods.jsClick(extensionDealersPage.createDealerBtn);
        methods.clickElement(driver, extensionDealersPage.createDealerBtn);
        String lastNameWarn = methods.getElementText(driver, usersPage.warnings);
        Assert.assertEquals("The field is required", lastNameWarn);

        methods.inputValue(driver, methods.randomNameLastName(6), usersPage.lastNameFld);
        methods.clearFieldLoop(driver, usersPage.emailFld);
        methods.clickElement(driver, extensionDealersPage.createDealerBtn);
        String emptyEmailWarn = methods.getElementText(driver, usersPage.warnings);
        Assert.assertEquals("The field is required", emptyEmailWarn);

        methods.inputValue(driver, "asadadad.com", usersPage.emailFld);
        methods.clickElement(driver, extensionDealersPage.createDealerBtn);
        String invalidEmailWarn = methods.getCustomAttribute(driver, usersPage.emailFld, "validationMessage");
        System.out.println(invalidEmailWarn);
        Assert.assertTrue(invalidEmailWarn.contains("Please include an '@' in the email address"));

        methods.clearFieldLoop(driver, usersPage.emailFld);
        methods.inputValue(driver, methods.randomEmail(), usersPage.emailFld);
        methods.clearFieldLoop(driver, usersPage.phoneField);
        methods.clickElement(driver, extensionDealersPage.createDealerBtn);
        String emptyPhoneWarn = methods.getElementText(driver, usersPage.phoneFlwWarnings);
        Assert.assertEquals("The field is required", emptyPhoneWarn);

        methods.inputValue(driver, "999999", usersPage.phoneField);
        methods.clickElement(driver, extensionDealersPage.createDealerBtn);
        String invalidPhoneWarn = methods.getElementText(driver, usersPage.phoneFlwWarnings);
        Assert.assertEquals("The value should be a valid phone number", invalidPhoneWarn);

        methods.clearFieldLoop(driver, usersPage.phoneField);
        methods.inputValue(driver, methods.randomPhone(), usersPage.phoneField);
        methods.clearFieldLoop(driver, usersPage.passwordFld);
        methods.clickElement(driver, extensionDealersPage.createDealerBtn);
        String emptyPassWarn = methods.getElementText(driver, usersPage.warnings);
        Assert.assertEquals("The field is required", emptyPassWarn);

        methods.inputValue(driver, Methods.getVariable("ShortPassword"), usersPage.passwordFld);
        methods.clearFieldLoop(driver, usersPage.rePasswordFld);
        methods.inputValue(driver, Methods.getVariable("ShortPassword"), usersPage.rePasswordFld);
        methods.clickElement(driver, extensionDealersPage.createDealerBtn);
        String shortPassWarn = methods.getElementText(driver, usersPage.warnings);
        Assert.assertEquals("The password should be min 8 chars and max 64 chars length", shortPassWarn);

        methods.clearFieldLoop(driver, usersPage.passwordFld);
        methods.inputValue(driver, Methods.getVariable("LongPassword"), usersPage.passwordFld);
        methods.inputValue(driver, Methods.getVariable("LongPassword"), usersPage.rePasswordFld);
        methods.clickElement(driver, extensionDealersPage.createDealerBtn);
        String longPassWarn = methods.getElementText(driver, usersPage.warnings);
        Assert.assertEquals("The password should be min 8 chars and max 64 chars length", longPassWarn);

        methods.clearFieldLoop(driver, usersPage.passwordFld);
        methods.clearFieldLoop(driver, usersPage.rePasswordFld);
        methods.inputValue(driver, "password", usersPage.passwordFld);
        methods.inputValue(driver, "passwor", usersPage.passwordFld);
        methods.clickElement(driver, extensionDealersPage.createDealerBtn);
        String mismatchWarn = methods.getElementText(driver, usersPage.warnings);
        Assert.assertEquals("The passwords are mismatched", mismatchWarn);
    }

    @Test
    public void dealerSearch() throws Exception {
        leads.login(Methods.getVariable("Platform"), Methods.getVariable("AdminLogin"), Methods.getVariable("AdminPassword"));
        extensionDealersPage.openDealersSubTab(driver);
        usersPage.usersSearch(Methods.getVariable("DealerFullName"));
        methods.waitForNumberOfElementsLessThen(extensionDealersPage.dealersListByName, 2, 20);
        ArrayList<String> dealerByName = Methods.getItemsList(driver, extensionDealersPage.dealersListByName);
        String dealerName = dealerByName.get(0);
        Assert.assertEquals(Methods.getVariable("DealerFullName"), dealerName);

        methods.clickElement(driver, usersPage.clearSearchBtn);
        methods.waitForNumberOfElementsMoreThan(extensionDealersPage.dealersListByEmail, 5, 20);
        usersPage.usersSearch(Methods.getVariable("DealerEmail"));
        methods.waitForNumberOfElementsLessThen(extensionDealersPage.dealersListByEmail, 2, 20);
        ArrayList<String> dealerByPhone = Methods.getItemsList(driver, extensionDealersPage.dealersListByEmail);
        String dealerPhone = dealerByPhone.get(0);
        Assert.assertEquals(Methods.getVariable("DealerEmail"), dealerPhone);
    }

    @Test
    public void sortByCopartLogin() throws Exception {
        leads.login(Methods.getVariable("Platform"), Methods.getVariable("AdminLogin"), Methods.getVariable("AdminPassword"));
        extensionDealersPage.openDealersSubTab(driver);
        methods.clickElement(driver, usersPage.sortByBtn);
        methods.clickElement(driver, extensionDealersPage.sortByDropdown);
        methods.waitForAllElementsAreVisible(extensionDealersPage.sortByList, 10);
        String copartLogin1 = Methods.chooseItemFromTheList(driver, extensionDealersPage.sortByList, 0);
        System.out.println(copartLogin1);
        methods.waitMethMS(500);
        methods.clickElement(driver, extensionDealersPage.confirmCopartChooseBtn);
        methods.waitForElementIsNotVisible(extensionDealersPage.confirmCopartChooseBtn, 20);
        ArrayList<String> copartLoginsList = Methods.getItemsList(driver, extensionDealersPage.dealersListByCopartLogin);
        System.out.println(copartLoginsList);
        //Assert.assertThat(copartLoginsList, Every.everyItem(Matchers.containsString(copartLogin1)));
        Assert.assertThat(copartLoginsList, Every.everyItem(Matchers.isOneOf(copartLogin1, "Blocked")));

        methods.clickElement(driver, usersPage.sortByBtn);
        methods.clickElement(driver, extensionDealersPage.sortByDropdown);
        methods.waitForAllElementsAreVisible(extensionDealersPage.sortByList, 10);
        String copartLogin2 = Methods.chooseItemFromTheList(driver, extensionDealersPage.sortByList, 1);
        methods.waitMethMS(500);
        methods.clickElement(driver, extensionDealersPage.confirmCopartChooseBtn);
        methods.waitForElementIsNotVisible(extensionDealersPage.confirmCopartChooseBtn, 20);
        ArrayList<String> copartLoginsList1 = Methods.getItemsList(driver, extensionDealersPage.dealersListByCopartLogin);
        //Assert.assertThat(copartLoginsList1, Every.everyItem(Matchers.equalTo(copartLogin2)));
        Assert.assertThat(copartLoginsList1, Every.everyItem(Matchers.isOneOf(copartLogin2, "Blocked")));
    }

    @Ignore
    @Test
    public void ffwef() throws Exception {
        leads.login(Methods.getVariable("Platform"), Methods.getVariable("AdminLogin"), Methods.getVariable("AdminPassword"));
        methods.clickElement(driver, extensionDealersPage.extensionTab);
        WebElement element = driver.findElement(By.xpath("//a[normalize-space()='Fees']"));
        element.click();
        WebElement table = driver.findElement(By.xpath("//div[@class='fee-item list-grouped__item']"));
        WebElement button = driver.findElement(By.xpath("//i[@class='fee-item__btn-icon ui-icon ui-icon_pen-fill']"));
        By ele = By.xpath("//i[@class='fee-item__btn-icon ui-icon ui-icon_pen-fill']");
        By fldValue = By.xpath("//span[normalize-space()='Georgia (GA)']//following-sibling::label/input");
        String fees = "$98";
        String fees1 = "$89";

//        boolean isPresented = methods.isElementPresent(ele);
//
//        if(isPresented == true) {
//            button.click();
//        }
//
//        methods.clearFieldLoop(driver, fldValue);
        By georgia = By.xpath("//div[@class='fees-cmn__row']//div[2]");// И так для каждого блока georgia california ets
        String fee = methods.getElementText(driver, georgia);
        System.out.println(fee);
        Assert.assertTrue(fee.contains(fees) && fee.contains(fees1));
    }


//    @After
//    public void shutDown() {
//        driver.quit();
//    }


}
