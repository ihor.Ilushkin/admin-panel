import org.hamcrest.Matchers;
import org.hamcrest.core.Every;
import org.junit.*;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.Reader;
import java.util.ArrayList;
import java.util.Base64;
import java.util.concurrent.TimeUnit;

public class ExtensionFeesTests {

    WebDriver driver;
    Methods methods;
    ClientPage clientPage;
    Leads leads;
    ExtensionDealersPage extensionDealersPage;
    ExtensionFeesPage extensionFeesPage;
    UsersPage usersPage;

    @Rule
    public ScreenshotRule screenshotRule = new ScreenshotRule();

    @Before
    public void setUp() throws Exception {
        driver = new ChromeDriver();
        screenshotRule.setDriver(driver);
        driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
        driver.manage().timeouts().pageLoadTimeout(60, TimeUnit.SECONDS);
        driver.manage().window().maximize();
        driver.get(Methods.getVariable("AdminPanelURL"));
        methods = new Methods(driver);
        clientPage = new ClientPage(driver);
        leads = new Leads(driver);
        extensionDealersPage = new ExtensionDealersPage(driver);
        extensionFeesPage = new ExtensionFeesPage(driver);
        usersPage = new UsersPage(driver);
    }
    // У новосозданного дилера дефолтные фии, проверка зайти в юзерс и попытаться найти этого дилера, если его нет в списке, то все ок
    // Добавляем этому дилеру кастномные фии и проверяем что он появился во вкладке юзерс с этими фим
    // Удаляем кастомные фии и его фи становится дефолтным, и соответственно он изчезает их вкладки юзерс


    @Test
    public void addManageRemoveGeneralFee() throws Exception {
        leads.login(Methods.getVariable("Platform"), Methods.getVariable("AdminLogin"), Methods.getVariable("AdminPassword"));
        extensionFeesPage.openDealersSubTab(driver);
        boolean isLblPresented = methods.isElementPresent(extensionFeesPage.destinationLbl);
        if (isLblPresented) {
            extensionFeesPage.removeGeneralFee(driver, 1, 2);
        }
        // Create fees
        extensionFeesPage.addNewFee(driver, 0, 1, 2);
        ArrayList<String> newJersey = Methods.getItemsList(driver, extensionFeesPage.newJerseyFees);
        Assert.assertThat(newJersey, Every.everyItem(Matchers.containsString(Methods.getVariable("RegularFee"))));
        ArrayList<String> texas = Methods.getItemsList(driver, extensionFeesPage.texasFees);
        Assert.assertThat(texas, Every.everyItem(Matchers.containsString(Methods.getVariable("RegularFee"))));
        ArrayList<String> georgia = Methods.getItemsList(driver, extensionFeesPage.georgiaFees);
        Assert.assertThat(georgia, Every.everyItem(Matchers.containsString(Methods.getVariable("RegularFee"))));
        ArrayList<String> florida = Methods.getItemsList(driver, extensionFeesPage.floridaFees);
        Assert.assertThat(florida, Every.everyItem(Matchers.containsString(Methods.getVariable("RegularFee"))));
        ArrayList<String> california = Methods.getItemsList(driver, extensionFeesPage.californiaFees);
        Assert.assertThat(california, Every.everyItem(Matchers.containsString(Methods.getVariable("RegularFee"))));

        // Edit fees
        methods.clickElement(driver, extensionFeesPage.editFeeBtn);
        extensionFeesPage.editFee(driver, 0, 1, 2);
        methods.waitMethMS(1000);
        ArrayList<String> newJerseyUpd = Methods.getItemsList(driver, extensionFeesPage.newJerseyFees);
        Assert.assertThat(newJerseyUpd, Every.everyItem(Matchers.containsString(Methods.getVariable("NewFee"))));
        ArrayList<String> texasUpd = Methods.getItemsList(driver, extensionFeesPage.texasFees);
        Assert.assertThat(texasUpd, Every.everyItem(Matchers.containsString(Methods.getVariable("NewFee"))));
        ArrayList<String> georgiaUpd = Methods.getItemsList(driver, extensionFeesPage.georgiaFees);
        Assert.assertThat(georgiaUpd, Every.everyItem(Matchers.containsString(Methods.getVariable("NewFee"))));
        ArrayList<String> floridaUpd = Methods.getItemsList(driver, extensionFeesPage.floridaFees);
        Assert.assertThat(floridaUpd, Every.everyItem(Matchers.containsString(Methods.getVariable("NewFee"))));
        ArrayList<String> californiaUpd = Methods.getItemsList(driver, extensionFeesPage.californiaFees);
        Assert.assertThat(californiaUpd, Every.everyItem(Matchers.containsString(Methods.getVariable("NewFee"))));

        // Remove fees
        extensionFeesPage.removeGeneralFee(driver, 1, 2);
        methods.waitForElementIsVisible(extensionFeesPage.createNewFeeBtn, 20);
        WebElement feeButton = driver.findElement(extensionFeesPage.createNewFeeBtn);
        Assert.assertTrue(feeButton.isDisplayed());
    }

    @Test
    public void dealerFeeManagement() throws Exception {
        String code = "+995 Georgia (საქართველო)";
        leads.login(Methods.getVariable("Platform"), Methods.getVariable("AdminLogin"), Methods.getVariable("AdminPassword"));
        // Default fees
        extensionDealersPage.openDealersSubTab(driver);
        ArrayList<String> dealersData = extensionDealersPage.fullDealerCreation(driver, 1, code);
        String dealerName = dealersData.get(0);
        methods.clickElement(driver, extensionFeesPage.closeTooltip);
        extensionFeesPage.feeUsersTab();
        ArrayList<String> firstUserName = Methods.getItemsList(driver, extensionFeesPage.dealersListByName);
        Assert.assertFalse(firstUserName.contains(dealerName));

        // Custom fees
        methods.clickElement(driver, extensionFeesPage.newFeeBtn);
        methods.clickElement(driver, extensionFeesPage.chooseDealerDropdown);
        methods.inputValue(driver, dealerName, extensionFeesPage.searchDealerFld);
        methods.waitForNumberOfElementsToBe(extensionFeesPage.chooseDealerList, 1, 20);
        Methods.chooseItemFromTheList(driver, extensionFeesPage.chooseDealerList, dealerName);
        extensionFeesPage.editFeeForDealer(driver, 0, 1, 2);
        ArrayList<String> feePageDealersList = Methods.getItemsList(driver, extensionFeesPage.dealersListByName);
        String upperDealer = feePageDealersList.get(0);
        Assert.assertTrue(upperDealer.contains(dealerName));

        // Remove custom fees
        extensionFeesPage.removeGeneralFee(driver, 1, 2);
        methods.waitForElementIsNotVisible(extensionFeesPage.updateFeeBtn, 20);
        ArrayList<String> feePageDealersListAfterFeeDeletion = Methods.getItemsList(driver, extensionFeesPage.dealersListByName);
        String newUpperDealer = feePageDealersListAfterFeeDeletion.get(0);
        Assert.assertFalse(newUpperDealer.contains(dealerName));

        // Remove dealer
        methods.clickElement(driver, extensionDealersPage.dealersSubTab);
        Methods.waitForElementInTheListText(driver, dealerName, extensionDealersPage.dealersListByName, extensionDealersPage.dealersManageCredentialsListButtons);
        extensionDealersPage.deleteDealer(driver);
        ArrayList<String> dealerTabDealersList = Methods.getItemsList(driver, extensionDealersPage.dealersListByName);
        Assert.assertFalse(dealerTabDealersList.contains(dealerName));
    }

    @Test
    public void search() throws Exception {
        leads.login(Methods.getVariable("Platform"), Methods.getVariable("AdminLogin"), Methods.getVariable("AdminPassword"));
        extensionDealersPage.openDealersSubTab(driver);
        methods.clickElement(driver, extensionFeesPage.searchButton);
        methods.inputValue(driver, Methods.getVariable("FeeUsersSearchFullName"), extensionFeesPage.searchField);
        methods.waitForNumberOfElementsToBe(extensionFeesPage.dealersSearchList, 1, 20);
        ArrayList<String> dealerNames = Methods.getItemsList(driver, extensionFeesPage.dealersSearchList);
        String dealerName = dealerNames.get(0);
        Assert.assertEquals(Methods.getVariable("FeeUsersSearchFullName"), dealerName);

        methods.clickElement(driver, extensionFeesPage.clearSearchButton);
        methods.waitForNumberOfElementsMoreThan(extensionFeesPage.dealersSearchList, 5, 20);
        methods.clickElement(driver, extensionFeesPage.searchButton);
        methods.inputValue(driver, Methods.getVariable("FeeUsersSearchEmail"), extensionFeesPage.searchField);
        methods.waitForNumberOfElementsToBe(extensionFeesPage.dealersEmailsSearchList, 1, 20);
        ArrayList<String> dealerEmails = Methods.getItemsList(driver, extensionFeesPage.dealersEmailsSearchList);
        String dealerEmail = dealerEmails.get(0);
        Assert.assertEquals(Methods.getVariable("FeeUsersSearchEmail"), dealerEmail);
    }

//    @After
//    public void shutDown() {
//        driver.quit();
//    }

}
