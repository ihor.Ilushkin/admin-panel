import org.apache.commons.io.FileUtils;
import org.junit.rules.TestWatcher;
import org.junit.runner.Description;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import java.time.LocalDate;
import java.time.LocalTime;

import java.io.File;
import java.io.IOException;
import java.time.Instant;
import java.time.LocalDate;
import java.util.Date;

public class ScreenshotRule extends TestWatcher {
    WebDriver driver;

    public void setDriver(WebDriver driver) {
        this.driver = driver;
    }

    @Override
    protected void finished(Description description) {
        System.out.println(description.getDisplayName() + " finished");

//        if (driver != null) {
//            driver.quit();
//        }
    }

    @Override
    protected void failed(Throwable e, Description description) {
        TakesScreenshot takesScreenshot = (TakesScreenshot) driver;

        File scrFile = takesScreenshot.getScreenshotAs(OutputType.FILE);
        File destFile = getDestinationFile(description.getDisplayName());
        try {
            FileUtils.copyFile(scrFile, destFile);
        } catch (IOException ioe) {
            throw new RuntimeException(ioe);
        }
    }

    private File getDestinationFile(String filename) {
        LocalDate date = LocalDate.now();
        LocalTime time = LocalTime.now();
        String userDirectory = FileUtils.getUserDirectoryPath();
        String fileName = filename + date + " " + time + " " + "-screenShot.png";
        String absoluteFileName = userDirectory + "/IdeaProjects/AdminPanel/screenshots/" + fileName;

        return new File(absoluteFileName);
    }
}

