import org.openqa.selenium.*;
import org.openqa.selenium.html5.LocalStorage;
import org.openqa.selenium.html5.WebStorage;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.FluentWait;
import org.openqa.selenium.support.ui.Wait;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.awt.*;
import java.awt.event.KeyEvent;
import java.io.FileInputStream;
import java.io.InputStreamReader;
import java.text.DecimalFormat;
import java.time.Duration;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;
import java.util.Random;
import java.util.function.Function;

public class Methods {

    WebDriver driver;

    public Methods(WebDriver driver) {
        this.driver = driver;
    }


    public boolean isElementClickable(By elementXPath) {
        WebDriverWait wait = new WebDriverWait(driver, 1);
        try {
            wait.until(ExpectedConditions.elementToBeClickable(elementXPath));
            return true;
        } catch (Exception e) {
            return false;
        }
    }

    public boolean isElementPresent(By locatorKey) {
        try {
            driver.findElement(locatorKey);
            return true;
        } catch (org.openqa.selenium.NoSuchElementException e) {
            return false;
        }
    }

    public boolean isElementPresentInTheList(WebDriver driver, By elementLocator, String text) {
        List<WebElement> elementList = driver.findElements(elementLocator);

        for (int i = 0; i < elementList.size(); i++) {
            WebElement element = elementList.get(i);
            String currentText = element.getText();
            if (currentText.contains(text)) {
                return true;
            }
        }
        return false;
    }

    public String randomNameLastName(int length) {
        final String data = "0123456789abcdefghijklmnopqrstuvwxyz";

        Random random = new Random();
        StringBuilder sb = new StringBuilder(length);

        for (int i = 0; i < length; i++)
            sb.append(data.charAt(random.nextInt(data.length())));
        return "AutoTest - " + sb.toString();
    }

    public String randomPhone() {
        Random rand = new Random();
        int num1 = 123; //(rand.nextInt(7) + 1) * 100 + (rand.nextInt(8) * 10) + rand.nextInt(8);
        int num2 = rand.nextInt(743);
        int num3 = rand.nextInt(10000);

        DecimalFormat df3 = new DecimalFormat("000"); // 3 zeros
        DecimalFormat df4 = new DecimalFormat("0000"); // 4 zeros

        String phoneNumber = df3.format(num1) + "-" + df3.format(num2) + "-" + df4.format(num3);

        return phoneNumber;
    }


    public String randomEmail() {
        int length = 5;
        String name = "0123456789abcdefghijklmnopqrstuvwxyz";
        String firstDomain = "0123456789abcdefghijklmnopqrstuvwxyz";
        String secondDomain = "abcdefghijklmnopqrstuvwxyz";

        Random random = new Random();
        String email = new String();

        StringBuilder sb = new StringBuilder(length);
        for (int i = 0; i < length; i++)
            sb.append(name.charAt(random.nextInt(name.length())));
        email += sb;
        email += "@mailinator.com";
        return email;
    }

    public void waitMethMS(int millis) {
        try {
            Thread.sleep(millis);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    public WebElement waitForElementIsVisible(By locator, int maxSeconds) {
        return (new WebDriverWait(driver, maxSeconds)).until(ExpectedConditions.visibilityOfElementLocated(locator));
    }


    public List<WebElement> waitForAllElementsAreVisible(By locator, int maxSeconds) {
        return (new WebDriverWait(driver, maxSeconds)).until(ExpectedConditions.visibilityOfAllElementsLocatedBy(locator));
    }


    public WebElement waitForElementIsPresented(By locator, int maxSeconds) {
        return (new WebDriverWait(driver, maxSeconds)).until(ExpectedConditions.presenceOfElementLocated(locator));
    }

    public Boolean waitForAttributeValueContains(By locator, String attribute, String value, int maxSeconds) {
        return (new WebDriverWait(driver, maxSeconds)).until(ExpectedConditions.attributeContains(locator, attribute, value));
    }

    public Boolean waitForAttributeToBe(By locator, String attribute, String value, int maxSeconds) {
        return (new WebDriverWait(driver, maxSeconds)).until(ExpectedConditions.attributeToBe(locator, attribute, value));
    }


    public List<WebElement> waitForNumberOfElementsToBe(By locator, int number, int maxSeconds) {
        return (new WebDriverWait(driver, maxSeconds)).until(ExpectedConditions.numberOfElementsToBe(locator, number));
    }

    public List<WebElement> waitForNumberOfElementsLessThen(By locator, int number, int maxSeconds) {
        return (new WebDriverWait(driver, maxSeconds)).until(ExpectedConditions.numberOfElementsToBeLessThan(locator, number));
    }

    public List<WebElement> waitForNumberOfElementsMoreThan(By locator, int number, int maxSeconds) {
        return (new WebDriverWait(driver, maxSeconds)).until(ExpectedConditions.numberOfElementsToBeMoreThan(locator, number));
    }

    public Boolean waitForTextToBePresentedInElement(By elementLocator, String elementText, int maxSeconds) {
        return (new WebDriverWait(driver, maxSeconds)).until(ExpectedConditions.textToBePresentInElementLocated(elementLocator, elementText));
    }

    public Boolean waitForTextToBePresentedInElement(WebElement element, String elementText, int maxSeconds) {
        return (new WebDriverWait(driver, maxSeconds)).until(ExpectedConditions.textToBePresentInElement(element, elementText));
    }

    public WebElement waitForElementIsClickable(By locator, int maxSeconds) {
        return (new WebDriverWait(driver, maxSeconds)).until(ExpectedConditions.elementToBeClickable(locator));
    }


    public Boolean waitForUrlContains(String url, int maxSeconds) {
        return (new WebDriverWait(driver, maxSeconds)).until(ExpectedConditions.urlContains(url));
    }

    public Boolean waitForElementIsNotVisible(By locator, int maxSeconds) {
        return (new WebDriverWait(driver, maxSeconds)).until(ExpectedConditions.invisibilityOfElementLocated(locator));
    }

    public Boolean waitForAllElementsNotVisible(By locator, int maxSeconds) {
        return (new WebDriverWait(driver, maxSeconds)).until(ExpectedConditions.invisibilityOfElementLocated(locator));
    }

    public Boolean waitForElementSelectionState(By elementLocator, String text, int maxSeconds) {
        return (new WebDriverWait(driver, maxSeconds)).until(ExpectedConditions.invisibilityOfElementWithText(elementLocator, text));
    }

    public Boolean waitForTitleIs(String title, int maxSeconds) {
        return (new WebDriverWait(driver, maxSeconds)).until(ExpectedConditions.titleIs(title));
    }

    public Boolean waitForTitleContains(String title, int maxSeconds) {
        return (new WebDriverWait(driver, maxSeconds)).until(ExpectedConditions.titleContains(title));
    }

    public static ArrayList<String> getItemsList(WebDriver driver, By elementXPath) {
        List<WebElement> listElements = driver.findElements(elementXPath);
        ArrayList<String> listItems = new ArrayList<>();

        for (int i = 0; i < listElements.size(); i++) {
            WebElement element = listElements.get(i);
            String itemText = element.getAttribute("textContent");
            listItems.add(itemText.trim());
        }
        return listItems;
    }

    public static ArrayList<String> getItemsList(WebDriver driver, By elementXPath, String attributeValue) {
        List<WebElement> listElements = driver.findElements(elementXPath);
        ArrayList<String> listItems = new ArrayList<>();

        for (int i = 0; i < listElements.size(); i++) {
            WebElement element = listElements.get(i);
            String itemText = element.getAttribute(attributeValue);
            listItems.add(itemText.trim());
        }
        return listItems;
    }


    public static void chooseItemFromTheList(WebDriver driver, By elementXPath, String itemValue) {

        List<WebElement> listElements = driver.findElements(elementXPath);

        for (int i = 0; i < listElements.size(); i++) {
            WebElement element = listElements.get(i);
            String itemText = element.getAttribute("textContent");
            if (itemText.contains(itemValue)) {
                element.click();

                break;
            }
        }
    }

    public static String chooseItemFromTheList(WebDriver driver, By elementXPath, int itemIndex) {

        List<WebElement> listElements = driver.findElements(elementXPath);
        WebElement element = listElements.get(itemIndex);
        String elementText = element.getText();
        element.click();
        return elementText;
    }


    public static String chooseRandomListItem(WebDriver driver, By elementXPath) {
        List<WebElement> listElements = driver.findElements(elementXPath);

        Random random = new Random();
        int randomIndex = random.nextInt(listElements.size());
        WebElement element = listElements.get(randomIndex);
        String itemText = element.getAttribute("textContent");
        element.click();

        return itemText;
    }

    public static ArrayList<Integer> convertStringListToInt(WebDriver driver, By elementXPath) {
        List<WebElement> prices = driver.findElements(elementXPath);
        ArrayList<Integer> actualPrices = new ArrayList<>();

        for (int i = 0; i < prices.size(); i++) {
            String convertedPrice = prices.get(i).getAttribute("textContent")
                    .replace(",", "")
                    .replace("$", "")
                    .replace("Bid", "")
                    .trim();
            actualPrices.add(Integer.parseInt(convertedPrice));
        }
        return actualPrices;
    }

    public static void switchToNextTab(WebDriver driver) {
        for (String tab : driver.getWindowHandles()) {
            driver.switchTo().window(tab);
        }
    }

    public static void switchToPreviousTab(WebDriver driver, String tabName) {
        driver.switchTo().window(tabName);
    }

    public static void removeItems(WebDriver driver, By itemsList) {
        List<WebElement> list = driver.findElements(itemsList);

        for (int i = 0; i < list.size(); i++) {

            WebElement element = list.get(i);
            if (element.isEnabled()) {
                element.click();
                try {
                    Thread.sleep(1000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            } else return;

        }

    }

    public void OpenNewTab() throws Exception {

        Robot robot = new Robot();
        robot.keyPress(KeyEvent.VK_CONTROL);
        robot.keyPress(KeyEvent.VK_T);
        robot.keyRelease(KeyEvent.VK_CONTROL);
        robot.keyRelease(KeyEvent.VK_T);
        ArrayList<String> tabs = new ArrayList<>(driver.getWindowHandles());
        System.out.println(tabs.toString());
    }

    public static void waitForElementInTheList(WebDriver driver, String ID, By elementXPath) {
        Methods methods = new Methods(driver);

        for (int i = 0; i < 30; i++) {
            List<WebElement> list = driver.findElements(elementXPath);

            for (int j = 0; j < list.size(); j++) {
                WebElement element = list.get(j);
                String lotID = element.getAttribute("href");

                if (lotID.contains(ID)) {
                    list.get(j).click();

                    return;
                }
            }
            methods.waitMethMS(4000);
            driver.navigate().refresh();

        }

    }

    public static void waitForElementInTheListText(WebDriver driver, String ID, By elementXPath) {
        Methods methods = new Methods(driver);

        for (int i = 0; i < 50; i++) {
            List<WebElement> list = driver.findElements(elementXPath);

            for (int j = 0; j < list.size(); j++) {
                try {
                    WebElement element = list.get(j);
                    String lotID = element.getText();
                    if (lotID.contains(ID)) {
                        element.click();

                        return;
                    }
                } catch (StaleElementReferenceException e) {
                }

            }
            methods.waitMethMS(1000);
            driver.navigate().refresh();

        }

    }

    public static void waitForElementInTheListText(WebDriver driver, String ID, By elementXPath, By elementXPath1) {
        Methods methods = new Methods(driver);

        for (int i = 0; i < 50; i++) {
            List<WebElement> list = driver.findElements(elementXPath);
            List<WebElement> buttonsList = driver.findElements(elementXPath1);

            for (int j = 0; j < list.size(); j++) {
                try {
                    WebElement element = list.get(j);
                    String lotID = element.getText();
                    if (lotID.contains(ID)) {
                        buttonsList.get(j).click();

                        return;
                    }
                } catch (StaleElementReferenceException e) {
                }

            }
            methods.waitMethMS(1000);
            driver.navigate().refresh();

        }

    }

    public boolean retryingFindClick(By by) {
        boolean result = false;
        int attempts = 0;
        while (attempts < 2) {
            try {
                driver.findElement(by).click();
                result = true;
                break;
            } catch (StaleElementReferenceException e) {
            }
            attempts++;
        }
        return result;
    }

    public static String getVariable(String param) throws Exception {
        Properties props = new Properties();
        props.load(new InputStreamReader(new FileInputStream("resources/variables.properties"), "UTF-8"));
        return props.getProperty(param);
    }

    public void clickElement(WebDriver driver, By elementLocator) {
        waitForElementIsVisible(elementLocator, 20);
        waitForElementIsClickable(elementLocator, 20);
        waitForElementInterceptedIgnore(driver, elementLocator);
    }

    public void RegularElementClick(WebDriver driver, By elementLocator) {
        waitForElementIsVisible(elementLocator, 20);
        waitForElementIsClickable(elementLocator, 20);
        driver.findElement(elementLocator).click();
    }

    public String getElementText(WebDriver driver, By elementLocator) {
        Methods methods = new Methods(driver);
        methods.waitForElementIsVisible(elementLocator, 20);
        return driver.findElement(elementLocator).getText();
    }

    public void inputValue(WebDriver driver, String value, By elementLocator) {
        waitForElementIsVisible(elementLocator, 20);
        driver.findElement(elementLocator).sendKeys(value);
    }

    public String getElementAttribute(WebDriver driver, By elementLocator) {
        waitForElementIsPresented(elementLocator, 20);
        return driver.findElement(elementLocator).getAttribute("class");
    }

    public String getCustomAttribute(WebDriver driver, By elementLocator, String attributeName) {
        waitForElementIsPresented(elementLocator, 20);
        return driver.findElement(elementLocator).getAttribute(attributeName);
    }

    public String getFieldValue(WebDriver driver, By elementLocator) {
        waitForElementIsPresented(elementLocator, 20);
        return driver.findElement(elementLocator).getAttribute("value");
    }

    public void clearField(WebDriver driver, By elementLocator) {
        waitForElementIsClickable(elementLocator, 20);
        waitMethMS(200);
        driver.findElement(elementLocator).sendKeys(Keys.COMMAND, "a");
        waitMethMS(200);
        driver.findElement(elementLocator).sendKeys(Keys.DELETE);
    }

    public void clearFieldLoop(WebDriver driver, By elementLocator) {
        WebElement field = driver.findElement(elementLocator);

        for (int i = 0; i < 70; i++) {
            field.sendKeys(Keys.BACK_SPACE);
            field.sendKeys(Keys.DELETE);
        }
    }

    public void clearFieldFastLoop(WebDriver driver, By elementLocator) {
        WebElement field = driver.findElement(elementLocator);

        for (int i = 0; i < 10; i++) {
            field.sendKeys(Keys.BACK_SPACE);
            field.sendKeys(Keys.DELETE);
        }
    }

    public void pageRefresh() {
        waitMethMS(1000);
        driver.navigate().refresh();
    }

    public void hoverOverElement(WebDriver driver, By elementLocator) {
        Actions action = new Actions(driver);
        WebElement element = driver.findElement(elementLocator);
        action.moveToElement(element).build().perform();

    }

    public void hoverOverElement(WebDriver driver, WebElement button) {
        Actions action = new Actions(driver);
        action.moveToElement(button).build().perform();

    }

    public static void checkboxClick(WebElement element, WebDriver driver) {
        Actions action = new Actions(driver);
        action.moveToElement(element).click().build().perform();
    }

    public void clickOnThePage() throws AWTException {
        Actions actions = new Actions(driver);

        Robot robot = new Robot();

        robot.mouseMove(1306, 357);

        actions.click().build().perform();

        robot.mouseMove(1306, 357);

        actions.click().build().perform();
    }

    public void clickOnThePage(int a, int b, int c, int d) throws AWTException {
        Actions actions = new Actions(driver);

        Robot robot = new Robot();

        robot.mouseMove(a, b);

        actions.click().build().perform();

        robot.mouseMove(c, d);

        actions.click().build().perform();
    }

    public void jsClick(By elementLocator) {
        WebElement element = driver.findElement(elementLocator);
        JavascriptExecutor executor = (JavascriptExecutor) driver;
        executor.executeScript("arguments[0].click();", element);
    }

    public static void moveTooltipToStorage(WebDriver driver) {
        LocalStorage local = ((WebStorage) driver).getLocalStorage();
        local.setItem("requestHelpShown", "yes");

    }

    public void interceptionIgnore() {
        Wait wait = new FluentWait(driver)
                .withTimeout(Duration.ofSeconds(20))
                .pollingEvery(Duration.ofSeconds(20))
                .ignoring(Exception.class);
    }

    public static WebElement waitForElementInterceptedIgnore(WebDriver driver, By elementLocator) {
        Wait<WebDriver> wait = new FluentWait<WebDriver>(driver)
                .withTimeout(Duration.ofSeconds(30))
                .pollingEvery(Duration.ofSeconds(1))
                .ignoring(ElementClickInterceptedException.class);

        return wait.until(new Function<WebDriver, WebElement>() {
            public WebElement apply(WebDriver driver) {
                WebElement element = driver.findElement(elementLocator);
                element.click();
                return element;
            }
        });
    }


}
