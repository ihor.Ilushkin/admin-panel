import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import java.util.ArrayList;
import java.util.List;

public class AccountsPage {

    WebDriver driver;

    public AccountsPage(WebDriver driver) {
        this.driver = driver;
    }

    public By accountsSubTab = By.xpath("//a[normalize-space()='Accounts']");

    // Sort by
    public By sortByButton = By.xpath("//div[@class='list-params__btn ui-button']/button");
    public By minimumLimitField = By.xpath("//input[@placeholder='Minimum limit']");
    public By maximumLimitField = By.xpath("//input[@placeholder='Maximum limit']");
    public By invalidCredentialsCheckbox = By.xpath("//span[@class='ui-check__frame']");
    public By resetAllOptionsButton = By.xpath("//button[contains(text(), 'Reset all options')]");
    public By setOptionsButton = By.xpath("//button[contains(text(), 'Set options')]");
    public By closeSortByModal = By.xpath("//div[@class='list-params-modal']/a/i");

    // Search
    public By searchButton = By.xpath("//div[@class='subnav-search__activate-btn ui-button']/button");
    public By searchField = By.xpath("//input[@placeholder='Type to search']");
    public By clearSearchButton = By.xpath("//button[@class='subnav-search__clear-btn']");

    // Main page
    public By dealersList = By.xpath("//i[@class='accounts-cmn__ico ui-icon ui-icon_user']");
    public By vehiclesList = By.xpath("//i[@class='accounts-cmn__ico ui-icon ui-icon_car-side']");
    public By bidLimitsList = By.xpath("//span[@class='btn-dropdown__toggle-txt']");
    public By bidLimitButton = By.xpath("//i[@class='btn-dropdown__toggle-arrow ui-icon ui-icon_arrow-down']");
    public By bidLimitField = By.xpath("//div[@class='btn-dropdown__content-main']//span[2]//following-sibling::input");
    public By bidLimitApplyButton = By.xpath("//button[normalize-space()='Apply']");
    public By feeStructureList = By.xpath("//p[@class='accounts-cmn__str accounts-cmn__str_sec']");
    public By statusList = By.xpath("//div[@class='account-cell-status account-item__cell']/p");
    public By copartAccountsList = By.xpath("//p[@class='account-cell-login__id cells-cmn__str cells-cmn__str_nb']");
    public By iosPicture = By.xpath("//i[@class='account-cell-login__icon account-cell-login__icon_mobile ui-icon ui-icon_mobile']");
    public By loading = By.xpath("//div[@class='list_items-wrp_loading']");

    // Copart account manage
    public By copartAccountManageButtonsList = By.xpath("//button[@class='account-cell-operations__dropdown-trigger']");
    public By changePasswordButton = By.xpath("//button[@class='op-edit-password account-op account-op__btn']");
    public By newPasswordField = By.xpath("//input[@placeholder='Pick a password']");
    public By reNewPasswordField = By.xpath("//input[@placeholder='Repeat the password']");
    public By newPasswordButton = By.xpath("//button[@type='submit']");
    public By notificationText = By.xpath("//p[@class='notification-item__text']");
    public By closeNotification = By.xpath("//button[@class='notification-item__close-btn']//i[@class='ui-icon ui-icon_close']");
    public By blockAccountButton = By.xpath("//button[@class='op-block account-op account-op__btn account-cell-operations__op']");
    public By enableIOSButton = By.xpath("//button[@class='op-ios account-op account-op__btn account-cell-operations__op']");

    public void openAccountSubTab() {
        Methods methods = new Methods(driver);
        ExtensionDealersPage extensionDealersPage = new ExtensionDealersPage(driver);
        methods.waitForElementIsVisible(extensionDealersPage.extensionTab, 20);
        methods.clickElement(driver, extensionDealersPage.extensionTab);
        methods.waitForElementIsVisible(accountsSubTab, 20);
        methods.clickElement(driver, accountsSubTab);
    }

    public void sortByBidLimit(String min, String max) {
        Methods methods = new Methods(driver);
        methods.clickElement(driver, sortByButton);
        methods.inputValue(driver, min, minimumLimitField);
        methods.inputValue(driver, max, maximumLimitField);
        methods.waitMethMS(500);
        methods.clickElement(driver, setOptionsButton);
        methods.waitForElementIsNotVisible(loading, 20);
    }

    public static ArrayList<Integer> convertStringListToInt(WebDriver driver, By elementXPath) {
        List<WebElement> bids = driver.findElements(elementXPath);
        ArrayList<Integer> actualBids = new ArrayList<>();

        for (int i = 0; i < bids.size(); i++) {
            String convertedPrice = bids.get(i).getAttribute("textContent")
                    .replace("$", "")
                    .trim();
            actualBids.add(Integer.parseInt(convertedPrice));
        }
        return actualBids;
    }

    public void changePassword(String password, String rePassword) throws Exception {
        Methods methods = new Methods(driver);
        methods.clickElement(driver, changePasswordButton);
        methods.inputValue(driver, password, newPasswordField);
        methods.inputValue(driver, rePassword, reNewPasswordField);
        methods.clickElement(driver, newPasswordButton);
    }

    public void enableDisableIOS () {
        Methods methods = new Methods(driver);
        methods.clickElement(driver, copartAccountManageButtonsList);
        methods.clickElement(driver, enableIOSButton);
    }

}
