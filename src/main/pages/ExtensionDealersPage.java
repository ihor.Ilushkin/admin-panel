import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class ExtensionDealersPage {

    WebDriver driver;

    public ExtensionDealersPage(WebDriver driver) {
        this.driver = driver;
    }

    // Tabs
    public By extensionTab = By.xpath("//a[normalize-space()='COPART EXTENSION']");
    public By dealersSubTab = By.xpath("//a[normalize-space()='Dealers']");

    // Create dealer modal
    public By createNewDealerBtn = By.xpath("//button[normalize-space()='New']");
    public By dealerCreationModalHeader = By.xpath("//h3[normalize-space()='Dealer create']");
    public By copartLoginBtn = By.xpath("//div[contains(text(), 'COPART LOGIN')]//button");
    public By copartLoginList = By.xpath("//div[contains(text(), 'COPART LOGIN')]//div[@class='ui-items__option']");
    public By createDealerBtn = By.xpath("//button[normalize-space()='Create dealer']");
    public By copartLogins = By.xpath("//div[@look='secondary']/div//div[@class='ui-items__option']");
    // Created dealer info modal
    public By copartLoginCreatedUserBtn = By.xpath("//span[@class='ui-select__button-lbl']");

    // Dealers list page
    public By dealersListByName = By.xpath("//span[contains(@class, 'dealer-cell-name-ava__link-txt')]");
    public By dealersListByEmail = By.xpath("//a[contains(@class, 'dealer-cell-email__link dealers-cmn__str')]");
    public By dealersListByCopartLogin = By.xpath("//p[@class='dealer-item__cell'][1]");
    public By dealersManageCredentialsListButtons = By.xpath("//button[@class='dealer-cell-operations__dropdown-trigger']");
    public By sortByDropdown = By.xpath("//button[@fill='frame']");
    public By sortByList = By.xpath("//div[@class='ui-items']//span");
    public By confirmCopartChooseBtn = By.xpath("//button[normalize-space()='Set options']");

    // Credentials managing window
    public By editCredentialsLink = By.xpath("//span[normalize-space()='Edit dealer details']");
    public By editPasswordLink = By.xpath("//span[normalize-space()='Edit dealer password']");
    public By blockTheDealerLink = By.xpath("//span[normalize-space()='Block the dealer']");
    public By deleteDealerLink = By.xpath("//span[normalize-space()='Delete dealer account']");
    public By confirmDeletion = By.xpath("//button[normalize-space()='Continue']");





    public ArrayList<String> fullDealerCreation(WebDriver driver, int copartLoginIndex, String code) {
        Methods methods = new Methods(driver);
        UsersPage usersPage = new UsersPage(driver);

        methods.clickElement(driver, createNewDealerBtn);
        String copartLogin = chooseDealerCopartLogin(driver, copartLoginBtn, copartLoginList, copartLoginIndex);
        methods.clickElement(driver, usersPage.codesDropdown);
        Methods.chooseItemFromTheList(driver, usersPage.codesList, code);
        String name = methods.randomNameLastName(6);
        String lastName = methods.randomNameLastName(6);
        String email = methods.randomEmail();
        String phone = usersPage.randomPhone();
        ArrayList<String> dealerCredentials = new ArrayList<>();
        Collections.addAll(dealerCredentials, name, lastName, email, phone, copartLogin);
        usersPage.userCreation(driver, name, lastName, email, phone, "password", "password");
        methods.clickElement(driver, createDealerBtn);
        //methods.clickElement(driver, usersPage.closeNotificationBtn);
        methods.waitForElementIsNotVisible(dealerCreationModalHeader, 20);
        return dealerCredentials;
    }

    public String chooseDealerCopartLogin(WebDriver driver, By dropdown, By dropdownList, int index) {
        Methods methods = new Methods(driver);
        String accountType = "";

        methods.clickElement(driver, dropdown);
        methods.waitForNumberOfElementsMoreThan(copartLogins, 1, 20);
        methods.waitForAllElementsAreVisible(dropdownList, 20);
        methods.waitMethMS(1000);
        accountType = Methods.chooseItemFromTheList(driver, dropdownList, index);

        return accountType;
    }

    public void openDealersSubTab(WebDriver driver) {
        Methods methods = new Methods(driver);
        methods.waitForElementIsVisible(extensionTab, 20);
        methods.clickElement(driver, extensionTab);
        methods.waitMethMS(500);
        methods.clickElement(driver, dealersSubTab);
    }

    public void deleteDealer(WebDriver driver) {
        Methods methods = new Methods(driver);
        methods.clickElement(driver, deleteDealerLink);
        methods.clickElement(driver, confirmDeletion);
        methods.waitMethMS(500);
    }

    public void choosePhoneCode(WebDriver driver, String code) {
        Methods methods = new Methods(driver);
        UsersPage usersPage = new UsersPage(driver);

        methods.clickElement(driver, usersPage.codesDropdown);
        methods.waitForAllElementsAreVisible(usersPage.codesList, 20);
        Methods.chooseItemFromTheList(driver, usersPage.codesList, code);
    }

    public String getDealerCopartLogin(WebDriver driver, By dealersList, String dealerName) {
        Methods methods = new Methods(driver);

        Methods.chooseItemFromTheList(driver, dealersList, dealerName);
        String currentCopartLogin = methods.getElementText(driver, copartLoginCreatedUserBtn);
        return currentCopartLogin;
    }



}
