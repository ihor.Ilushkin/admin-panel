import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Random;

public class UsersPage {

    public WebDriver driver;

    public UsersPage(WebDriver driver) {
        this.driver = driver;
    }

    public By usersTab = By.xpath("//i[@class='sidebar__link-icon ui-icon ui-icon_user-fill']");
    // User create modal
    public By newUserBtn = By.xpath("//button[normalize-space()='New user']");
    public By userCreateModalHeader = By.xpath("//h3[normalize-space()='User create']");
    public By accountTypeDropdown = By.xpath("//div[@class='ui-select select-account-type user-create-modal__field-input']//button[@type='button']");
    public By accountTypesList = By.xpath("//div[@class='ui-select__wrap ui-select_is-open']//div[@class='ui-select__dropdown']//div[@class='ui-select__options']//div//div[@class='ui-items']//div[@class='ui-items__option']");
    public By firstNameFld = By.xpath("//input[@placeholder='John']");
    public By middleNameFld = By.xpath("//input[@placeholder='Josh']");
    public By lastNameFld = By.xpath("//input[@placeholder='Doe']");
    public By emailFld = By.xpath("//input[@placeholder='address@example.com']");
    public By codesDropdown = By.xpath("//div[@class='ui-phone__country-dropdown-btn ui-button']//button");
    public By codesList = By.xpath("//div[@class='ui-phone__country-dropdown-item ui-button']");
    public By phoneField = By.xpath("//input[@placeholder='Phone Number']");
    public By passwordFld = By.xpath("//input[@placeholder='Pick a password']");
    public By rePasswordFld = By.xpath("//input[@placeholder='Repeat the password']");
    public By createUserBtn = By.xpath("//button[normalize-space()='Create user']");
    public By cancelBtn = By.xpath("//button[normalize-space()='Cancel']");
    public By warnings = By.xpath("//span[@class='ui-text__label ui-text__label_is-error']");
    public By phoneFlwWarnings = By.xpath("//span[@class='ui-phone__label ui-phone__label-error']");
    public By closeUserCreationModal = By.xpath("//button[@class='user-card-modal__close-btn']//i[@class='ui-icon ui-icon_close']");

    // Users list page
    public By usersListByName = By.xpath("//span[@class='cells-cmn__str cells-cmn__str_nb']"); // For all users tabs
    public By userListByPhone = By.xpath("//a[contains(@class, 'user-cell-phone__link users-cmn__str')]");
    public By userListByEmail = By.xpath("//a[contains(@class, 'l-email__link users-cmn__str')]");
    public By createdAtList = By.xpath("//span[contains(@class, 'user-cell-time-ago__link users-cmn__str')]");
    public By userManagingNotificationsText = By.xpath("//p[@class='notification-item__text']");
    public By closeNotificationBtn = By.xpath("//button[@class='notification-item__close-btn']//i[@class='ui-icon ui-icon_close']");

    // User credential manage window
    public By manageUserCredButtonsList = By.xpath("//button[@class='user-cell-operations__dropdown-trigger']");
    public By verifyEmailLink = By.xpath("//span[normalize-space()='Verify email']");
    public By verifyPhoneLink = By.xpath("//span[normalize-space()='Verify phone']");
    public By editUserCredentialsLink = By.xpath("//span[normalize-space()='Edit user credentials']");
    public By passwordChangeModalHeader = By.xpath("//h3[normalize-space()='Password change']");
    public By passwordChangeModalPasswordFld = By.xpath("//input[@placeholder='Pick a password']");
    public By passwordChangeModalRePasswordFld = By.xpath("//input[@placeholder='Repeat the password']");
    public By changePasswordBtn = By.xpath("//button[normalize-space()='Change password']");
    public By blockTheUserLink = By.xpath("//span[normalize-space()='Block the user']");
    public By deleteAccBtn = By.xpath("//span[normalize-space()='Delete user account']");
    public By confirmUserDeletionBtn = By.xpath("//button[normalize-space()='Continue']");
    public By createdUserModalTypeDropdown = By.xpath("//div[@class='ui-select select-account-type user-card-field__field-component']//button[@type='button']");
    public By userTypeLbl = By.xpath("//span[@class='ui-select__button-lbl']");

    // Header
    public By universalTabs = By.xpath("//div[@class='page-subnav__content-right']//a");
    public By usersHeaderTab = By.xpath("//div[@class='page-subnav__content']//a[normalize-space(text())='USERS']");
    public By proUsersHeaderTab = By.xpath("//a[normalize-space()='PRO-USERS']");
    public By brokersHeaderTab = By.xpath("//a[normalize-space()='BROKERS']");
    public By dealersHeaderTab = By.xpath("//a[normalize-space()='DEALERS']");
    public By orderByModal = By.xpath("//form[@class='list-params-form']");
    public By sortByBtn = By.xpath("//span[@class='list-params__btn-txt']");
    public By orderByDropdown = By.xpath("//div[@class='ui-select list-params-row__field']//button[@type='button']");
    public By orderByList = By.xpath("//label[normalize-space()='ORDER BY']//following-sibling::div//div[@class='ui-items__option']");
    public By setOptionBtn = By.xpath("//button[normalize-space()='Set options']");
    public By resetOptionBtn = By.xpath("//button[normalize-space()='Reset all options']");
    public By usersSearchBtn = By.xpath("//i[@class='subnav-search__activate-btn-ico ui-icon ui-icon_search']");
    public By usersSearchFld = By.xpath("//input[@placeholder='Type to search']");
    public By clearSearchBtn = By.xpath("//button[@class='subnav-search__clear-btn']");

    public String chooseUserType(WebDriver driver, By dropdown, By dropdownList, int index) {
        Methods methods = new Methods(driver);
        String accountType = "";

        methods.clickElement(driver, dropdown);
        methods.waitForAllElementsAreVisible(dropdownList, 20);
        accountType = Methods.chooseItemFromTheList(driver, dropdownList, index);

        return accountType;
    }

    public void userCreation(WebDriver driver, String name, String lastName, String email, String phone, String pass, String rePass) {
        Methods methods = new Methods(driver);
        methods.waitMethMS(500);
        methods.inputValue(driver, name, firstNameFld);
        methods.inputValue(driver, lastName, lastNameFld);
        methods.inputValue(driver, email, emailFld);
        methods.inputValue(driver, phone, phoneField);
        methods.inputValue(driver, pass, passwordFld);
        methods.inputValue(driver, rePass, rePasswordFld);
    }

    public String randomPhone() {
        Random rand = new Random();
        int num1 = 123;
        int num2 = rand.nextInt(100);
        int num3 = rand.nextInt(100);
        int num4 = rand.nextInt(100);

        DecimalFormat df3 = new DecimalFormat("00");

        String phoneNumber = df3.format(num1) + " " + df3.format(num2) + " " + df3.format(num3) + " " + df3.format(num4);

        return phoneNumber;
    }

    public String sortBy(WebDriver driver, By button, By dropdown, By dropdownList, int index, By setButton) {
        Methods methods = new Methods(driver);

        methods.clickElement(driver, button);
        methods.clickElement(driver, dropdown);
        methods.waitForAllElementsAreVisible(dropdownList, 10);
        String sortingByName = Methods.chooseItemFromTheList(driver, dropdownList, index);
        methods.clickElement(driver, setButton);
        return sortingByName;
    }

    public void deleteUser(By buttonsList, By deleteBtn, By confirmBtn) {
        Methods methods = new Methods(driver);

        List<WebElement> element = driver.findElements(buttonsList);
        element.get(0).click();
        methods.clickElement(driver, deleteBtn);
        methods.clickElement(driver, confirmBtn);
    }

    public void usersSearch(String searchData) {
        Methods methods = new Methods(driver);
        methods.clickElement(driver, usersSearchBtn);
        methods.waitForElementIsVisible(usersSearchFld, 10);
        methods.inputValue(driver, searchData, usersSearchFld);
    }

    public ArrayList<String> fullUsersCreation(WebDriver driver, int accountTypeIndex, String code) {
        Methods methods = new Methods(driver);
        UsersPage usersPage = new UsersPage(driver);

        methods.clickElement(driver, usersPage.newUserBtn);
        usersPage.chooseUserType(driver, usersPage.accountTypeDropdown, usersPage.accountTypesList, accountTypeIndex);
        methods.clickElement(driver, usersPage.codesDropdown);
        Methods.chooseItemFromTheList(driver, usersPage.codesList, code);
        String name = methods.randomNameLastName(6);
        String lastName = methods.randomNameLastName(6);
        String email = methods.randomEmail();
        String phone = usersPage.randomPhone();
        ArrayList<String> userCredentials = new ArrayList<>();
        Collections.addAll(userCredentials, name, lastName, email, phone);
        this.userCreation(driver, name, lastName, email, phone, "password", "password");
        methods.clickElement(driver, usersPage.createUserBtn);
        methods.clickElement(driver, usersPage.closeNotificationBtn);
        methods.waitForElementIsNotVisible(userCreateModalHeader, 20);
        return userCredentials;
    }

    public void changeUserPassword(String password, String rePassword) {
        Methods methods = new Methods(driver);

        methods.clickElement(driver, editUserCredentialsLink);
        methods.waitForElementIsVisible(passwordChangeModalHeader, 10);
        methods.inputValue(driver, password, passwordChangeModalPasswordFld);
        methods.inputValue(driver, rePassword, passwordChangeModalRePasswordFld);
        methods.clickElement(driver, changePasswordBtn);
    }


}
