import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import java.util.List;

public class Leads {

    public WebDriver driver;

    public Leads(WebDriver driver) {
        this.driver = driver;
    }

    public By platformFld = By.xpath("//input[@placeholder='Platform']");
    public By emailFld = By.xpath("//input[@placeholder='Email']");
    public By passwordFld = By.xpath("//input[@placeholder='Password']");
    public By submitBtn = By.xpath("//button[normalize-space()='Sign In']");
    public By unassignedTab = By.xpath("//a[normalize-space()='Unassigned']");
    public By assignedTab = By.xpath("//a[normalize-space()='Assigned']");
    //Create lead modal
    public By createLeadBtn = By.xpath("//button[normalize-space()='Create Lead']");
    public By channelDropdown = By.xpath("//div[@class='ui-dropdown__trigger ui-dropdown__trigger_select-like'][normalize-space()='Phone']");
    public By channelDropdownList = By.xpath("//div[contains(@class, 'ui-dropdown-item ui-dropdown-item_interactive')]");
    public By userFullNameFld = By.xpath("//div[@class='lead-create__form']//input[@class='ui-text__input']");
    public By countryCodesBtn = By.xpath("//div[@class='ui-flag ui-flag_ca']");
    public By countryCodesLst = By.xpath("//div[@class='ui-phone__country-dropdown-item ui-button']");
    public By phoneNumberFld = By.xpath("//input[@class='ui-phone__input']");
    public By createNewLeadBtn = By.xpath("//button[normalize-space()='Create new Lead']");
    public By leadItemUserName = By.xpath("//span[@class='lead-item__user-name-text']");
    public By testLead = By.xpath("//span[normalize-space()='Autotest AdminPanel']");
    //Lead modal
    public By leadHeaderUserName = By.xpath("//span[@class='lead-details__head-user-name']");
    public By leadModalEmailFld = By.xpath("//input[@placeholder='No email']");
    public By locationFld = By.xpath("//input[@placeholder='Location not specified']");
    public By financingToggleNo = By.xpath("//body/div[@id='app']/div[@class='layout']/div[@class='layout__modal']/div[@class='vue-portal-target']/div[@class='lead-overview lead-overview_animate']/div[@class='lead-overview__content-wrap']/div[@class='lead']/div[@class='lead-details__tab-wrap']/div[@class='lead-tab-user lead-details__tab-component']/div[@class='ui-row']/div[3]/div[1]//button[contains(text(), 'No')]");
    public By financingToggleYes = By.xpath("//body/div[@id='app']/div[@class='layout']/div[@class='layout__modal']/div[@class='vue-portal-target']/div[@class='lead-overview lead-overview_animate']/div[@class='lead-overview__content-wrap']/div[@class='lead']/div[@class='lead-details__tab-wrap']/div[@class='lead-tab-user lead-details__tab-component']/div[@class='ui-row']/div[3]/div[1]//button[contains(text(), 'Yes')]");
    public By becomeProToggleNo = By.xpath("//div[@class='layout__modal']//div[6]//button[contains(text(), 'No')]");
    public By highPriorityBtn = By.xpath("//i[@class='lead-priority-mark__ico ui-icon ui-icon_favourite']");
    public By becomeProToggleYes = By.xpath("//div[@class='layout__modal']//div[6]//button[contains(text(), 'Yes')]");
    public By calculatorTab = By.xpath("//button[normalize-space()='CARS & CALCULATOR']");
    public By addLotByVINFld = By.xpath("//div[@class='lot-finder lead-lot-add__form']//input");
    public By addACarBtn = By.xpath("//button[normalize-space()='Add Car']");
    public By vinCodeCalculator = By.xpath("//div[@class='lead-lot__info']//div[2]//div[3]//span[2]");
    public By addNewCarBtn = By.xpath("//div[contains(@class, 'slider-pager-btn_add')]/button");
    public By scrollLeftBtn = By.xpath("//div[contains(@class, 'slider-pager-btn_left')]/button");
    public By scrollRight = By.xpath("//div[contains(@class, 'slider-pager-btn_right')]/button");
    public By closeLeadModal = By.xpath("//button[@class='lead-overview__close-btn']//i[@class='ui-icon ui-icon_close']");
    public By closeLotInfoBlock = By.xpath("//div[@class='lead-lots-carousel__slider-item-remove ui-button']//i");
    public By changeLeadsStatusDropdown = By.xpath("//span[@class='lead-state-select__trigger-text']");
    public By changeLeadsStatusList = By.xpath("//div[contains(@class, 'ui-dropdown-item lead-state-select__item ui-dropdown-item')]");
    public By dueDateDoneBtn = By.xpath("//button[contains(text(),'Done')]");
    public By currentLeadStatus = By.xpath("//span[@class='lead-state-select__trigger-text']");
    public By assignToDropdownBtn = By.xpath("//div[@class='ui-dropdown lead-broker-select lead-details__footer-broker-select']//div[@class='ui-dropdown__trigger ui-dropdown__trigger_select-like']");
    public By brokersList = By.xpath("//div[@class='lead-broker-select__item-name']");
    public By currentBrokerName = By.xpath("//div[@class='lead-details__footer']//div[@class='lead-broker-select__current-name']");

    //Header
    public By sortByBtn = By.xpath("//span[@class='list-params__btn-txt']");
    public By orderByDropdown = By.xpath("//div[@class='list-params-sort list-params-row leads-list-params__row']//i[@class='ui-select__dock ui-select__dock_caret ui-icon ui-icon_dropdown']");
    public By orderByList = By.xpath("//div[@class='list-params-form__rows']/div[1]//div[@class='ui-select__dropdown']//div[@class='ui-items__option']");
    public By setOptionsBtn = By.xpath("//button[normalize-space()='Set options']");
    public By leadRequestCount = By.xpath("//div[@class='lead-item'] [1]//div[@class='lead-item__phone-missed-num']");
    public By resetAllOptionsBtn = By.xpath("//button[normalize-space()='Reset all options']");
    public By byChannelDropdown = By.xpath("//div[@class='list-params-channel list-params-row leads-list-params__row']//button[@type='button']");
    public By byChannelList = By.xpath("//label[normalize-space()='BY CHANNEL']/following-sibling::div//div[@class='ui-items__option']");
    public By byBroker = By.xpath("//div[@class='list-params-broker list-params-row leads-list-params__row']//button[@type='button']");
    public By byBrokerList = By.xpath("//div[@class='ui-select__wrap ui-select_is-open']//div[@class='ui-select__dropdown']//span[@class='ui-items__list-item-txt']");
    public By verifiedCheckbox = By.xpath("//span[normalize-space()='Verified']/preceding-sibling::span");
    public By unverifiedCheckbox = By.xpath("//span[normalize-space()='Unverified']/preceding-sibling::span");
    public By prioritizedCheckbox = By.xpath("//span[normalize-space()='Prioritized']/preceding-sibling::span");
    public By financedCheckbox = By.xpath("//span[normalize-space()='Financed']/preceding-sibling::span");
    public By proCandidateCheckbox = By.xpath("//span[normalize-space()='Pro candidate']/preceding-sibling::span");
    public By upperSearchField = By.xpath("//input[@placeholder='Search for Leads']");
    public By lowerSearchBtn = By.xpath("//i[@class='subnav-search__activate-btn-ico ui-icon ui-icon_search']");
    public By lowerSearchFld = By.xpath("//div[@class='subnav-search leads__subnav-action']/label/input[@class='ui-text__input']");
    public By closeLowerSearch = By.xpath("//button[@class='subnav-search__clear-btn']//i[@class='ui-icon ui-icon_close']");
    public By quickSearch = By.xpath("//div[@class='quick-search-lead__inner-row']");
    public By quickSearchName = By.xpath("//div[@class='quick-search-lead__inner']//span[@class='quick-search-lead__name']");
    public By quickSearchPhone = By.xpath("//div[@class='quick-search-lead__inner']//span[@class='quick-search-lead__phone']");
    public By searchClearBtn = By.xpath("//i[@class='quick-search-input__clear-btn-ico ui-icon ui-icon_close']");
    public By waitingForCallTab = By.xpath("//a[normalize-space()='WAITING FOR CALL']");
    public By onHoldTab = By.xpath("//a[normalize-space()='ON-HOLD']");
    public By lookingForACarTab = By.xpath("//a[normalize-space()='LOOKING FOR A CAR']");
    public By offerSent = By.xpath("//a[normalize-space()='OFFER SENT']");


    //Leads states
    public By allLeadsExceptRegular = By.xpath("//div[contains(@class, 'lead-item lead-item')]");
    public By regularLeadsList = By.xpath("//div[@class='lead-item']");
    public By highPriorityList = By.xpath("//div[@class='lead-item lead-item_high-priority']");
    public By financingLeadsList = By.xpath("//div[@class='lead-item lead-item_financing']");
    public By becomeProList = By.xpath("//div[@class='lead-item lead-item_interested-in-pro']");
    public By becomeProAndFinancingList = By.xpath("//div[@class='lead-item lead-item_interested-in-pro lead-item_financing']");
    public By becomeProFinancingPriorityList = By.xpath("//div[@class='lead-item lead-item_high-priority lead-item_interested-in-pro lead-item_financing']");
    public By channelPhone = By.xpath("//img[@src='/img/phone.7c95707d.svg']");
    public By channelTelegram = By.xpath("//img[@src='/img/telegram.b55bd499.svg']");
    public By channelWebSite = By.xpath("//i[@class='channel-icon__icon ui-icon ui-icon_link']");
    public By verifiedLeadsList = By.xpath("//span[contains(@class, 'lead-item-type_onboard')]");
    public By unverifiedLeadsList = By.xpath("//span[contains(@class, 'lead-item-type_guest')]");
    public By prioritizedLeadsList = By.xpath("//div[contains(@class, 'lead-item lead-item_high-priority')]");
    public By financedLeadsList = By.xpath("//div[contains(@class, 'lead-item_financing')]");
    public By interestedInProLeadsList = By.xpath("//div[contains(@class, 'lead-item_interested-in-pro')]");
    public By proLeadsList = By.xpath("//span[contains(@class, 'user-name-pro-mark')]");
    public By allLeads = By.xpath("//div[@class='lead-item' or @class='lead-item lead-item_high-priority' or @class='lead-item lead-item_high-priority lead-item_financing' or @class='lead-item lead-item_high-priority lead-item_interested-in-pro' or class='lead-item lead-item_high-priority lead-item_interested-in-pro lead-item_financing' or @class='lead-item lead-item_high-priority lead-item_interested-in-pro lead-item_financing']");
    public By allLeadsByName = By.xpath("//span[@class='lead-item__user-name-text']");
    public By allLeadsByBroker = By.xpath("//div[@class='lead-broker-select__current-name']");
    public By leadsVerification = By.xpath("//span[contains(@class, 'lead-item-type lead-item__user-type')]");

    public By allLeadsConcat = By.xpath("//div[contains(concat(' ', @class, ' '), ' lead-item ')]");//todo


    //ManageLeadModal
    //public By financingYes = By.xpath("")


    public void login(String platformName, String email, String password) {
        Methods methods = new Methods(driver);
        methods.inputValue(driver, platformName, platformFld);
        methods.inputValue(driver, email, emailFld);
        methods.inputValue(driver, password, passwordFld);
        methods.clickElement(driver, submitBtn);
    }

    public void leadCreation(String userName, String phoneNumber) {
        Methods methods = new Methods(driver);
        String code = "+995 Georgia (საქართველო)";

        driver.findElement(createLeadBtn).click();
        driver.findElement(channelDropdown).click();
        driver.findElement(userFullNameFld).sendKeys(userName);
        driver.findElement(countryCodesBtn).click();
        methods.waitForAllElementsAreVisible(countryCodesLst, 20);
        Methods.chooseItemFromTheList(driver, countryCodesLst, code);
        driver.findElement(phoneNumberFld).sendKeys(phoneNumber);
        methods.waitMethMS(1000);
        driver.findElement(createNewLeadBtn).click();
    }

    public static String waitForLead(WebDriver driver, By elementLocator, String leadName) {
        Methods methods = new Methods(driver);

        String name = "";

        for (int i = 0; i < 15; i++) {
            methods.waitMethMS(2000);
            WebElement element = driver.findElement(elementLocator);

            if (element.getText().contains(leadName)) {
                String amount = element.getText();
                name = amount;

                break;
            }
            driver.navigate().refresh();
        }
        return name;
    }

    public void switchLeadStatus(WebDriver driver, By leadName, By toggle) {
        Methods methods = new Methods(driver);
        methods.waitForElementIsVisible(leadName, 20);
        methods.clickElement(driver, leadName);
        methods.clickElement(driver, toggle);
    }

    public String chooseFilter(WebDriver driver, By filterDropdown, By filterList, int itemIndex) {
        Methods methods = new Methods(driver);
        methods.clickElement(driver, filterDropdown);
        methods.waitForAllElementsAreVisible(filterList, 20);
        methods.waitMethMS(500);
        String filterValue = Methods.chooseItemFromTheList(driver, filterList, itemIndex);
        methods.waitMethMS(1000);
        methods.clickElement(driver, setOptionsBtn);
        return filterValue;
    }

    public void chooseFilter(WebDriver driver, By filterDropdown, By filterList, String itemValue) {
        Methods methods = new Methods(driver);
        methods.clickElement(driver, filterDropdown);
        methods.waitForAllElementsAreVisible(filterList, 20);
        methods.waitMethMS(500);
        Methods.chooseItemFromTheList(driver, filterList, itemValue);
        methods.waitMethMS(1000);
        methods.clickElement(driver, setOptionsBtn);
    }

    public void chooseFilter(WebDriver driver, By filterList, String itemValue) {
        Methods methods = new Methods(driver);
        methods.waitForAllElementsAreVisible(filterList, 20);
        methods.waitMethMS(500);
        Methods.chooseItemFromTheList(driver, filterList, itemValue);
        methods.waitMethMS(1000);
        methods.clickElement(driver, setOptionsBtn);
    }

    public void resetFilters(WebDriver driver) {
        Methods methods = new Methods(driver);
        methods.clickElement(driver, sortByBtn);
        methods.waitForElementIsVisible(resetAllOptionsBtn, 20);
        methods.clickElement(driver, resetAllOptionsBtn);
        methods.waitMethMS(500);
    }

    public void chooseUserGroup(WebDriver driver, By userGroup) {
        Methods methods = new Methods(driver);

        methods.waitMethMS(500);
        methods.clickElement(driver, userGroup);
        methods.waitMethMS(500);
        methods.clickElement(driver, setOptionsBtn);
        methods.waitMethMS(500);
    }

    public void chooseUserGroup(WebDriver driver, By userGroup, By userGroup1) {
        Methods methods = new Methods(driver);

        methods.waitMethMS(500);
        driver.findElement(userGroup).click();
        methods.waitMethMS(500);
        driver.findElement(userGroup1).click();
        methods.waitMethMS(500);
        driver.findElement(setOptionsBtn).click();
        methods.waitMethMS(500);
    }

    public void ifTestLeadMissing(WebDriver driver, By leadsList, String testLead, String code) throws Exception {
        Methods methods = new Methods(driver);
        Leads leads = new Leads(driver);
        List<WebElement> lead = driver.findElements(leadsList);
        WebElement firstLead = lead.get(0);

        if (firstLead.getText().contains(testLead)) {
            return;
        } else {
            methods.clickElement(driver, leads.createLeadBtn);
            methods.waitMethMS(500);
            methods.inputValue(driver, Methods.getVariable("UserFullName"), leads.userFullNameFld);
            methods.clickElement(driver, leads.countryCodesBtn);
            Methods.chooseItemFromTheList(driver, leads.countryCodesLst, code);
            methods.inputValue(driver, Methods.getVariable("UserPhoneNoCode"), leads.phoneNumberFld);
            methods.clickElement(driver, leads.createNewLeadBtn);
            methods.clickElement(driver, leads.closeLeadModal);
        }
        return;
    }

    public void closeDueDateAndLead(WebDriver driver, By duDate, By close){
        Methods methods = new Methods(driver);
        methods.clickElement(driver, duDate);
        methods.waitMethMS(500);
        methods.clickElement(driver, close);
        methods.waitMethMS(500);
    }



}
