import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class ExtensionFeesPage {

    WebDriver driver;

    public ExtensionFeesPage(WebDriver driver) {
        this.driver = driver;
    }

    public By feesSubTab = By.xpath("//a[normalize-space()='Fees']");
    // Edit fee modal
    public By editFeeBtn = By.xpath("//button[@class='fee-item__btn']");
    public By destinationDropdown = By.xpath("//button[@fill='frame']");
    public By destinationList = By.xpath("//div[@class='ui-items']//div");
    public By georgiaFeeField = By.xpath("//span[normalize-space()='Georgia (GA)']/following-sibling::label/input");
    public By newYorkFeeField = By.xpath("//span[normalize-space()='New York (NY)']/following-sibling::label/input");
    public By washingtonFeeField = By.xpath("//span[normalize-space()='Washington (WA)']/following-sibling::label/input");
    public By californiaFeeField = By.xpath("//span[normalize-space()='California (CA)']/following-sibling::label/input");
    public By texasFeeField = By.xpath("//span[normalize-space()='Texas (TX)']/following-sibling::label/input");
    public By newJerseyFeeField = By.xpath("//span[normalize-space()='New Jersey (NJ)']/following-sibling::label/input");
    public By floridaFeeField = By.xpath("//span[normalize-space()='Florida (FL)']/following-sibling::label/input");
    public By updateFeeBtn = By.xpath("//button[normalize-space()='Update fee']");
    public By createFeeBtn = By.xpath("//button[normalize-space()='Create fee']");
    // Page elements
    public By destinationLbl = By.xpath("//span[normalize-space()='Georgia, Poti']");
    public By createNewFeeBtn = By.xpath("//span[normalize-space()='Create General Fee']");
    public By newJerseyFees = By.xpath("//div[@class='fee-item__cell'][2]");
    public By texasFees = By.xpath("//div[@class='fee-item__cell'][3]");
    public By georgiaFees = By.xpath("//div[@class='fee-item__cell'][4]");
    public By floridaFees = By.xpath("//div[@class='fee-item__cell'][5]");
    public By californiaFees = By.xpath("//div[@class='fee-item__cell'][6]");
    public By closeTooltip = By.xpath("//button[@class='notification-item__close-btn']//i[@class='ui-icon ui-icon_close']");
    // New fee modal
    public By newFeeBtn = By.xpath("//span[normalize-space()='New fee']");
    public By chooseDealerDropdown = By.xpath("//p[@class='dealer-select__current-name']");
    public By chooseDealerList = By.xpath("//p[@class='dealer-select__item-name']");
    public By searchDealerFld = By.xpath("//input[@placeholder='Search']");
    //public By createFeeBtn = By.xpath("//button[normalize-space()='Create fee']");

    // Fee users tab
    public By feeUsersTab = By.xpath("//a[@class='fees__tab']");
    public By dealersListByName = By.xpath("//p[@class='cells-cmn__str cells-cmn__str_nb']");
    public By dealersSearchList = By.xpath("//span[@class='cells-cmn__str cells-cmn__str_nb dealer-cell-name-ava__link-txt']");
    public By dealersEmailsSearchList = By.xpath("//a[contains(@class, 'dealers-cmn__str_nb dealers-cmn__str_sec')]");
    public By clearSearchButton = By.xpath("//button[@class='subnav-search__clear-btn']");
    public By dealersList = By.xpath("//a[@class='cells-cmn__str cells-cmn__str_nb']");

    // Search
    public By searchButton = By.xpath("//i[@class='subnav-search__activate-btn-ico ui-icon ui-icon_search']");
    public By searchField = By.xpath("//input[@placeholder='Type to search']");


    public void openDealersSubTab(WebDriver driver) {
        Methods methods = new Methods(driver);
        ExtensionDealersPage extensionDealersPage = new ExtensionDealersPage(driver);
        methods.clickElement(driver, extensionDealersPage.extensionTab);
        methods.clickElement(driver, feesSubTab);
    }


    public void removeGeneralFee(WebDriver driver, int destInd1, int destInd2) {
        Methods methods = new Methods(driver);

        methods.clickElement(driver, editFeeBtn);
        //methods.clearField(driver, georgiaFeeField);
        methods.clearFieldFastLoop(driver, newJerseyFeeField);
        //methods.clearField(driver, newYorkFeeField);
        methods.clearFieldFastLoop(driver, georgiaFeeField);
        //methods.clearField(driver, washingtonFeeField);
        methods.clearFieldFastLoop(driver, californiaFeeField);
        //methods.clearField(driver, californiaFeeField);
        methods.clearFieldFastLoop(driver, texasFeeField);
        //methods.clearField(driver, texasFeeField);
        methods.clearFieldFastLoop(driver, floridaFeeField);
        methods.waitMethMS(200);
        methods.clickElement(driver, destinationDropdown);
        methods.waitForAllElementsAreVisible(destinationList, 20);
        Methods.chooseItemFromTheList(driver, destinationList, destInd1);
        //methods.clearField(driver, newJerseyFeeField);
        methods.clearFieldFastLoop(driver, newJerseyFeeField);
        //methods.clearField(driver, georgiaFeeField);
        methods.clearFieldFastLoop(driver, georgiaFeeField);
        //methods.clearField(driver, californiaFeeField);
        methods.clearFieldFastLoop(driver, californiaFeeField);
        //methods.clearField(driver, texasFeeField);
        methods.clearFieldFastLoop(driver, texasFeeField);
        //methods.clearField(driver, floridaFeeField);
        methods.clearFieldFastLoop(driver, floridaFeeField);
        methods.waitMethMS(200);
        methods.clickElement(driver, destinationDropdown);
        methods.waitForAllElementsAreVisible(destinationList, 20);
        Methods.chooseItemFromTheList(driver, destinationList, destInd2);
        //methods.clearField(driver, newJerseyFeeField);
        methods.clearFieldFastLoop(driver, newJerseyFeeField);
        //methods.clearField(driver, georgiaFeeField);
        methods.clearFieldFastLoop(driver, georgiaFeeField);
        //methods.clearField(driver, californiaFeeField);
        methods.clearFieldFastLoop(driver, californiaFeeField);
        //methods.clearField(driver, texasFeeField);
        methods.clearFieldFastLoop(driver, texasFeeField);
        //methods.clearField(driver, floridaFeeField);
        methods.clearFieldFastLoop(driver, floridaFeeField);
        methods.waitMethMS(200);
        methods.clickElement(driver, updateFeeBtn);
    }

    public void addNewFee(WebDriver driver, int indexGeo, int indexUkr, int indexAz) throws Exception {
        Methods methods = new Methods(driver);
        methods.clickElement(driver, createNewFeeBtn);
        methods.clickElement(driver, destinationDropdown);
        methods.waitForAllElementsAreVisible(destinationList, 20);
        Methods.chooseItemFromTheList(driver, destinationList, indexGeo);
        methods.inputValue(driver, Methods.getVariable("RegularFee"), newJerseyFeeField);
        methods.inputValue(driver, Methods.getVariable("RegularFee"), georgiaFeeField);
        methods.inputValue(driver, Methods.getVariable("RegularFee"), californiaFeeField);
        methods.inputValue(driver, Methods.getVariable("RegularFee"), texasFeeField);
        methods.inputValue(driver, Methods.getVariable("RegularFee"), floridaFeeField);
        methods.clickElement(driver, destinationDropdown);
        methods.waitForAllElementsAreVisible(destinationList, 20);
        Methods.chooseItemFromTheList(driver, destinationList, indexUkr);
        methods.inputValue(driver, Methods.getVariable("RegularFee"), newJerseyFeeField);
        methods.inputValue(driver, Methods.getVariable("RegularFee"), georgiaFeeField);
        methods.inputValue(driver, Methods.getVariable("RegularFee"), californiaFeeField);
        methods.inputValue(driver, Methods.getVariable("RegularFee"), texasFeeField);
        methods.inputValue(driver, Methods.getVariable("RegularFee"), floridaFeeField);
        methods.clickElement(driver, destinationDropdown);
        methods.waitForAllElementsAreVisible(destinationList, 20);
        Methods.chooseItemFromTheList(driver, destinationList, indexAz);
        methods.inputValue(driver, Methods.getVariable("RegularFee"), newJerseyFeeField);
        methods.inputValue(driver, Methods.getVariable("RegularFee"), georgiaFeeField);
        methods.inputValue(driver, Methods.getVariable("RegularFee"), californiaFeeField);
        methods.inputValue(driver, Methods.getVariable("RegularFee"), texasFeeField);
        methods.inputValue(driver, Methods.getVariable("RegularFee"), floridaFeeField);
        methods.clickElement(driver, createFeeBtn);
    }

    public void editFee(WebDriver driver, int indexGeo, int indexUkr, int indexAz) throws Exception {
        Methods methods = new Methods(driver);
        methods.clickElement(driver, destinationDropdown);
        methods.waitForAllElementsAreVisible(destinationList, 20);
        Methods.chooseItemFromTheList(driver, destinationList, indexGeo);
        methods.clearField(driver, newJerseyFeeField);
        methods.inputValue(driver, Methods.getVariable("NewFee"), newJerseyFeeField);
        methods.clearField(driver, georgiaFeeField);
        methods.inputValue(driver, Methods.getVariable("NewFee"), georgiaFeeField);
        methods.clearField(driver, californiaFeeField);
        methods.inputValue(driver, Methods.getVariable("NewFee"), californiaFeeField);
        methods.clearField(driver, texasFeeField);
        methods.inputValue(driver, Methods.getVariable("NewFee"), texasFeeField);
        methods.clearField(driver, floridaFeeField);
        methods.inputValue(driver, Methods.getVariable("NewFee"), floridaFeeField);
        methods.clickElement(driver, destinationDropdown);
        methods.waitForAllElementsAreVisible(destinationList, 20);
        Methods.chooseItemFromTheList(driver, destinationList, indexUkr);
        methods.clearField(driver, newJerseyFeeField);
        methods.inputValue(driver, Methods.getVariable("NewFee"), newJerseyFeeField);
        methods.clearField(driver, georgiaFeeField);
        methods.inputValue(driver, Methods.getVariable("NewFee"), georgiaFeeField);
        methods.clearField(driver, californiaFeeField);
        methods.inputValue(driver, Methods.getVariable("NewFee"), californiaFeeField);
        methods.clearField(driver, texasFeeField);
        methods.inputValue(driver, Methods.getVariable("NewFee"), texasFeeField);
        methods.clearField(driver, floridaFeeField);
        methods.inputValue(driver, Methods.getVariable("NewFee"), floridaFeeField);
        methods.clickElement(driver, destinationDropdown);
        methods.waitForAllElementsAreVisible(destinationList, 20);
        Methods.chooseItemFromTheList(driver, destinationList, indexAz);
        methods.clearField(driver, newJerseyFeeField);
        methods.inputValue(driver, Methods.getVariable("NewFee"), newJerseyFeeField);
        methods.clearField(driver, georgiaFeeField);
        methods.inputValue(driver, Methods.getVariable("NewFee"), georgiaFeeField);
        methods.clearField(driver, californiaFeeField);
        methods.inputValue(driver, Methods.getVariable("NewFee"), californiaFeeField);
        methods.clearField(driver, texasFeeField);
        methods.inputValue(driver, Methods.getVariable("NewFee"), texasFeeField);
        methods.clearField(driver, floridaFeeField);
        methods.inputValue(driver, Methods.getVariable("NewFee"), floridaFeeField);
        methods.clickElement(driver, updateFeeBtn);
    }

    public void editFeeForDealer(WebDriver driver, int indexGeo, int indexUkr, int indexAz) throws Exception {
        Methods methods = new Methods(driver);
        methods.clickElement(driver, destinationDropdown);
        methods.waitForAllElementsAreVisible(destinationList, 20);
        Methods.chooseItemFromTheList(driver, destinationList, indexGeo);
        methods.clearField(driver, newJerseyFeeField);
        methods.inputValue(driver, Methods.getVariable("NewFee"), newJerseyFeeField);
        methods.clearField(driver, georgiaFeeField);
        methods.inputValue(driver, Methods.getVariable("NewFee"), georgiaFeeField);
        methods.clearField(driver, californiaFeeField);
        methods.inputValue(driver, Methods.getVariable("NewFee"), californiaFeeField);
        methods.clearField(driver, texasFeeField);
        methods.inputValue(driver, Methods.getVariable("NewFee"), texasFeeField);
        methods.clearField(driver, floridaFeeField);
        methods.inputValue(driver, Methods.getVariable("NewFee"), floridaFeeField);
        methods.clickElement(driver, destinationDropdown);
        methods.waitForAllElementsAreVisible(destinationList, 20);
        Methods.chooseItemFromTheList(driver, destinationList, indexUkr);
        methods.clearField(driver, newJerseyFeeField);
        methods.inputValue(driver, Methods.getVariable("NewFee"), newJerseyFeeField);
        methods.clearField(driver, georgiaFeeField);
        methods.inputValue(driver, Methods.getVariable("NewFee"), georgiaFeeField);
        methods.clearField(driver, californiaFeeField);
        methods.inputValue(driver, Methods.getVariable("NewFee"), californiaFeeField);
        methods.clearField(driver, texasFeeField);
        methods.inputValue(driver, Methods.getVariable("NewFee"), texasFeeField);
        methods.clearField(driver, floridaFeeField);
        methods.inputValue(driver, Methods.getVariable("NewFee"), floridaFeeField);
        methods.clickElement(driver, destinationDropdown);
        methods.waitForAllElementsAreVisible(destinationList, 20);
        Methods.chooseItemFromTheList(driver, destinationList, indexAz);
        methods.clearField(driver, newJerseyFeeField);
        methods.inputValue(driver, Methods.getVariable("NewFee"), newJerseyFeeField);
        methods.clearField(driver, georgiaFeeField);
        methods.inputValue(driver, Methods.getVariable("NewFee"), georgiaFeeField);
        methods.clearField(driver, californiaFeeField);
        methods.inputValue(driver, Methods.getVariable("NewFee"), californiaFeeField);
        methods.clearField(driver, texasFeeField);
        methods.inputValue(driver, Methods.getVariable("NewFee"), texasFeeField);
        methods.clearField(driver, floridaFeeField);
        methods.inputValue(driver, Methods.getVariable("NewFee"), floridaFeeField);
        methods.clickElement(driver, createFeeBtn);
        methods.waitForElementIsNotVisible(createFeeBtn, 20);
    }

    public void feeUsersTab () {
        Methods methods = new Methods(driver);
        methods.clickElement(driver, feesSubTab);
        methods.waitForElementIsClickable(feeUsersTab, 20);
        methods.clickElement(driver, feeUsersTab);
    }


}
