import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.html5.LocalStorage;
import org.openqa.selenium.html5.WebStorage;

import java.util.List;
import java.util.Random;

public class ClientPage {

    public WebDriver driver;

    public ClientPage(WebDriver driver) {
        this.driver = driver;
    }

    public By profileBtn = By.xpath("//button[@class='profile-panel__badge']");
    public By emailPhoneFld = By.xpath("//input[@placeholder='Email or phone number']");
    public By passwordFld = By.xpath("//input[@placeholder='Password']");
    public By logInBtn = By.xpath("//button[@type='submit'][normalize-space()='Log In']");
    public By loginWarningMessage = By.xpath("//p[@class='sign-in-form__error auth-form-error']");
    public By rightSideBarHeader = By.xpath("//p[@class='profile-head__user-info-name']");
    public By verifyEmailBtn = By.xpath("//button[@data-test-id='profile-edit-info/verify-email-btn']");
    public By verifyPhoneBtn = By.xpath("//button[@data-test-id='profile-edit-info/verify-phone-btn']");
    public By clientWarnings = By.xpath("//p[@class='toast-message__txt']");

    public By catalogueLink = By.xpath("//p[normalize-space()='Catalogue']");
    public By typeDropdown = By.xpath("//input[@placeholder='Type']");
    public By typedList = By.xpath("//div[@class='options-list__item-wrp']");
    public By otherFiltersLink = By.xpath("//button[normalize-space()='Other Filters']");
    public By locationDropdown = By.xpath("//input[@placeholder='Location']");
    public By locationList = By.xpath("//div[@class='options-list__item-wrp']");
    public By applyBtn = By.xpath("//div[@class='ui-multi-select-v2__apply']/button");
    public By lotsList = By.xpath("//div[@class='ui-list__item ui-list__item_absolute']");
    public By vinNumber = By.xpath("//div[@data-test-id='car-details/row/vin-code']//following-sibling::span[@class='car-details__row-val']");

    public By logoutBtn = By.xpath("//button[@data-test-id='profile-links/sign-out']");

    public void login(WebDriver driver, By menu, By loginFld, String login, By passFld, String pass, By confBtn) {
        Methods methods = new Methods(driver);

        methods.waitForElementIsVisible(menu, 20);
        driver.findElement(menu).click();
        methods.waitMethMS(500);
        methods.waitForElementIsVisible(loginFld, 20);
        driver.findElement(loginFld).sendKeys(login);
        driver.findElement(passFld).sendKeys(pass);
        methods.waitMethMS(200);
        driver.findElement(confBtn).click();
    }

    public void login(WebDriver driver, String login, String password) {
        Methods methods = new Methods(driver);
        methods.inputValue(driver, login, emailPhoneFld);
        methods.inputValue(driver, password, passwordFld);
        methods.clickElement(driver, logInBtn);
    }

    public void logout() {
        Methods methods = new Methods(driver);

        methods.clickElement(driver, profileBtn);
        methods.clickElement(driver, logoutBtn);
    }

    public void selectRandomLotFromCatalogue(WebDriver driver, By elementXpath) {
        List<WebElement> allProducts = driver.findElements(elementXpath);
        Random rand = new Random();
        int randomProduct = rand.nextInt(allProducts.size());
        allProducts.get(randomProduct).click();
    }

    public void getLotInsideUSCars(WebDriver driver, By lotsList) {
        Methods methods = new Methods(driver);
        driver.findElement(catalogueLink).click();
        driver.findElement(typeDropdown).click();
        methods.waitForAllElementsAreVisible(typedList, 20);
        Methods.chooseItemFromTheList(driver, typedList, 3);
        methods.waitMethMS(500);
        driver.findElement(applyBtn).click();
        driver.findElement(otherFiltersLink).click();
        methods.waitForAllElementsAreVisible(lotsList, 20);
        driver.findElement(locationDropdown).click();
        methods.waitForAllElementsAreVisible(locationList, 20);
        Methods.chooseItemFromTheList(driver, locationList, 0);
        driver.findElement(applyBtn).click();
        methods.waitMethMS(500);
        methods.waitForAllElementsAreVisible(lotsList, 20);
        this.selectRandomLotFromCatalogue(driver, lotsList);
    }

    public void moveTooltipToStorage(WebDriver driver) {
        LocalStorage local = ((WebStorage) driver).getLocalStorage();
        local.setItem("requestHelpShown", "yes");

    }

    public void clearLoginAndPass(WebDriver driver) {
        Methods methods = new Methods(driver);
        methods.clearFieldLoop(driver, emailPhoneFld);
        methods.clearFieldLoop(driver, passwordFld);
    }

    public boolean isVerified(WebDriver driver, By elementLocator) {
        Methods methods = new Methods(driver);

        methods.clickElement(driver, profileBtn);
        methods.clickElement(driver, rightSideBarHeader);
        boolean isPresented = methods.isElementPresent(elementLocator);
        return isPresented;

    }


}
